"""
Planning class
This class implements the planning algorithms to discovery services.

Author: Christian Cabrera
"""

import json
import time
import os
import math
import copy
import logic.FeedbackManager as feedbackManager

from entities.Plan import Plan
from entities.SimpleStep import SimpleStep
from entities.SequentialStep import SequentialStep
from entities.ParallelStep import ParallelStep
from entities.PlanGraph import PlanGraph
from entities.Vertex import Vertex
from entities.Edge import Edge


from random import choice
from pprint import pprint
from pymongo import MongoClient
from owlready2 import *
from numpy import number

"""Path to ontologies folder"""
onto_path.append("../../Data/ontologies/owls-tc4")

"""
Words to remove from the service/request parameters description
"""
to_remove = ["output","input","service","of","a","an","at","the","on","in","is","are"]
ignore = ["thebestservice","europricewhiskeycoffeeservice","trafficinformationservice.","authorpublicationmaxpriceservice",
          "breadbiscuitpriceservice","monographpublisherservice","authorbooktaxfreepriceservice","grocerystoresandwichquantityservice",
          "irishcoffeeforpriceservice","bookpricetaxedpriceservice","availablepreparedfoodservice","authorpublicationtaxfreepriceservice",
          "postalcodesofplacesfinderservice","dfkiservice","novelpriceservice","bookpriceservice"]

services2000File = []
"""with open("../../Data/services-dataset/services-2000.json") as dataFile:    
    services2000File = json.load(dataFile)"""

services4000File = []
"""with open("../../Data/services-dataset/services-4000.json") as dataFile:    
    services4000File = json.load(dataFile)"""

services6000File = []
"""with open("../../Data/services-dataset/services-6000.json") as dataFile:    
    services6000File = json.load(dataFile)"""

services8000File = []
"""with open("../../Data/services-dataset/services-8000.json") as dataFile:    
    services8000File = json.load(dataFile)"""
    
services10000File = []
"""with open("../../Data/services-dataset/services-10000.json") as dataFile:    
    services10000File = json.load(dataFile)"""

"""
Backward planning algorithm using hybrid matchmaker.
@jsonRequest: Original request in JSON format.
@previousPlans: Collection of plans previously discovered in other gateways.
@matchingType: Matchmaking type:
    0 - Semantic matchmaker (Logic).
    1 - Cosine Similarity (Non-logic).
    2 - Jaccard Index (Non-Logic).
    3 - Semantic matchmaker and Cosine Similarity (Hybrid).
    4 - Semantic matchmaker and Jaccard Index (Hybrid).
@simThreshold : Similarity threshold. 
@return List of discovered plans.
"""
def backwardPlanningClassic(jsonRequest,previousPlans,matchingType,similarityThreshold,counter):
    print("Backward planning: " + jsonRequest["id"] + ":: Counter: " + str(counter))
    counter = counter + 1
    if counter>=10:
        return previousPlans
    plans = {}
    """ Create new plan or use previous"""
    if(len(previousPlans) == 0):
        plan = PlanGraph(0, {}, [], 0.0, [], [], [])
        initial = Vertex(0, "initial", 1, {}, [], [], jsonRequest["inputs"][:], jsonRequest["domains"][:])
        initial.setState(1);
        plan.getVertexs()[initial.getName()] = initial
        final = Vertex(1, "final", 0, {}, jsonRequest["outputs"][:], jsonRequest["outputs"][:], [], jsonRequest["domains"][:])
        final.setRemainingInputs(jsonRequest["outputs"][:])
        plan.getVertexs()[final.getName()] = final
        plan.setInputs(jsonRequest["inputs"][:])
        plan.setOutputs(jsonRequest["outputs"][:])
        plan.setCurrentInputs(jsonRequest["outputs"][:])
        plan.setState(0)
        for domain in jsonRequest["domains"]:
            if domain not in plan.getDomains():
                plan.getDomains().append(domain)
        previousPlans[len(previousPlans)] = plan
        
    """Graph creation"""
    for kp, plan in previousPlans.items():
        requestOutputs = plan.getCurrentInputs()
        requestInputs = plan.getInputs()
        """Defining search space"""
        searchSpace = servicesFile
                          
        """Compare each output of each service with each remaining input on each remaining vertex"""
        for service in searchSpace:
            
            tempPlan = copy.deepcopy(plan)
            addTemp = False
            discoveredVertex = Vertex(-1, "", 0, {}, [], [], [], [])
            if service["name"] in tempPlan.getVertexs().keys():
                discoveredVertex = tempPlan.getVertexs()[service["name"]]
            else:
                discoveredVertex.setType(2)
                discoveredVertex.setName(service["name"])
                discoveredVertex.setState(0)
                discoveredVertex.setService(service)
                discoveredVertex.setInputs(service["inputs"][:])
                discoveredVertex.setRemainingInputs(service["inputs"][:])
                discoveredVertex.setOutputs(service["outputs"][:])
                discoveredVertex.setDomains(service["domains"][:])
            remainingVertexs = tempPlan.getRemainingVertexs()
            comparison = hybridMatchmaker2(remainingVertexs, discoveredVertex, matchingType, similarityThreshold)        
            if comparison["match"] == True:
                edges = comparison["edges"]
                for edge in edges:
                    link = edge.getLink()
                    tempPlan.getVertexs()[service["name"]] = discoveredVertex
                    tempPlan.update(edge.getDestination(), edge.getSource(), link["matchedInput"], link["matchedOutput"])
                    tempPlan.addEdge(edge)
                    for domain in service["domains"]:
                        if domain not in tempPlan.getDomains():
                            plan.getDomains().append(domain)
                    addTemp = True
            if addTemp:
                initial = tempPlan.getVertexs()["initial"]
                init = False
                remainingVertexsTemp = tempPlan.getRemainingVertexs()
                comparison = hybridMatchmaker2(remainingVertexsTemp, initial, matchingType, similarityThreshold)        
                if comparison["match"] == True:
                    init = True
                    edges = comparison["edges"]
                    for edge in edges:
                        link = edge.getLink()
                        tempPlan.update(edge.getDestination(), edge.getSource(), link["matchedInput"], link["matchedOutput"])
                        tempPlan.addEdge(edge)
                if len(tempPlan.getRemainingVertexs()) == 0 and init == True:
                    tempPlan.setState(1)
                    plans[len(plans)] = copy.deepcopy(tempPlan)
                else:
                    #if len(tempPlan.getVertexs()) - 2 <= 5:
                    if True:
                        tempPlans = {}
                        re = {}
                        tempPlans[len(tempPlans)] = copy.deepcopy(tempPlan)
                        """re = backwardPlanningClassic(jsonRequest, tempPlans, matchingType, similarityThreshold, counter)
                        tempPlans = re["plans"]
                        counter = re ["counter"]"""
                        tempPlans = backwardPlanningClassic(jsonRequest, tempPlans, matchingType, similarityThreshold, counter)
                        for k3, p in tempPlans.items():
                            if p.getState() == 1:
                                plans[len(plans)] = copy.deepcopy(p)
    """ret = {}
    ret["plans"] = plans
    ret["counter"] = counter"""
    return plans
    
"""
Backward planning algorithm reducing search space with Mongo DB queries and using knowledge about previous discovery processes.
@jsonRequest: Original request in JSON format.
@previousPlans: Collection of plans previously discovered in other gateways.
@matchingType: Matchmaking type:
    5 - Semantic matchmaker (Logic).
    6 - Cosine Similarity (Non-logic).
    7 - Jaccard Index (Non-Logic).
    8 - Semantic matchmaker and Cosine Similarity (Hybrid).
    9 - Semantic matchmaker and Jaccard Index (Hybrid).
@similarityThreshold : Threshold for syntactic matching.
@feedbackThreshold: Threshold to check in discovered edges.
@top : K plans to be choosen.
@functionalThreshold: Threshold to check to add a discovered plan in the list of top K plans.
@servicesPerGateway : Defines with file of services to read in TCHPC experiments.
@return Discovered plans according to functional requirements.
"""
def backwardPlanningHeuristic(jobId,jsonRequest,previousPlans,matchingType,similarityThreshold,feedbackThreshold, top, functionalThreshold, servicesPerGateway,feedt,tk):
    #print("Backward planning: " + jsonRequest["id"])
    plans = {}
    """ Create new plan or use previous"""
    if(len(previousPlans) == 0):
        plan = PlanGraph(0, {}, [], 0.0, [], [], [])
        initial = Vertex(0, "initial", 1, {}, [], [], jsonRequest["inputs"][:], jsonRequest["domains"][:])
        initial.setState(1);
        plan.getVertexs()[initial.getName()] = initial
        final = Vertex(1, "final", 0, {}, jsonRequest["outputs"][:], jsonRequest["outputs"][:],[], jsonRequest["domains"][:])
        final.setRemainingInputs(jsonRequest["outputs"][:])
        plan.getVertexs()[final.getName()] = final
        plan.setInputs(jsonRequest["inputs"][:])
        plan.setOutputs(jsonRequest["outputs"][:])
        plan.setCurrentInputs(jsonRequest["outputs"][:])
        plan.setState(0)
        for domain in jsonRequest["domains"]:
            if domain not in plan.getDomains():
                plan.getDomains().append(domain)
        previousPlans[len(previousPlans)] = plan
        
    """Graph creation"""
    tempPlans = {}
    planChanged = False
    np = 0
    for kp, plan in previousPlans.items():
        np = np + 1
        requestOutputs = plan.getCurrentInputs()[:]
        requestInputs = plan.getInputs()[:]
        """Defining search space"""
        parametersTypes = getParameterTypes(requestOutputs)
        searchSpace = []
        servicesNames = []
        #searchSpace = services.find({ "outputs.type": {"$in" : parametersTypes } })
        if servicesPerGateway == 2000:
            servicesFile = services2000File
        
        if servicesPerGateway == 4000:
            servicesFile = services4000File
        
        if servicesPerGateway == 6000:
            servicesFile = services6000File
        
        if servicesPerGateway == 8000:
            servicesFile = services8000File
        
        if servicesPerGateway == 10000:
            servicesFile = services10000File
        
        for service in servicesFile:
            add = False
            for parameterType in parametersTypes:
                serviceOutputs = []
                serviceOutputs = service["outputs"]
                for output in serviceOutputs:
                    if output["type"] == parameterType:
                        add = True
                        break
            if add:
                service["name"] = service["name"].replace(".","")
                if service["name"] not in servicesNames :
                    searchSpace.append(service)
                    servicesNames.append(service["name"])
            
        if len(searchSpace) == 0:
            servicesList = servicesFile
            for service in servicesList:
                service["name"] = service["name"].replace(".","")
                if service["name"] not in servicesNames:
                    searchSpace.append(service)
                    servicesNames.append(service["name"])
                          
        """Compare each output of each service with each remaining input on each remaining vertex"""
        for service in searchSpace:
            tempPlan = copy.deepcopy(plan)
            addTemp = False
            discoveredVertex = Vertex(-1, "", 0, {}, [], [], [], [])
            if service["name"] in tempPlan.getVertexs().keys():
                discoveredVertex = tempPlan.getVertexs()[service["name"]]
            else:
                discoveredVertex.setType(2)
                discoveredVertex.setName(service["name"])
                discoveredVertex.setState(0)
                discoveredVertex.setService(service)
                discoveredVertex.setInputs(service["inputs"][:])
                discoveredVertex.setRemainingInputs(service["inputs"][:])
                discoveredVertex.setOutputs(service["outputs"][:])
                discoveredVertex.setDomains(service["domains"][:])
            remainingVertexs = tempPlan.getRemainingVertexs()
            comparison = hybridMatchmaker2(remainingVertexs, discoveredVertex, matchingType, similarityThreshold)        
            if comparison["match"] == True:
                edges = comparison["edges"]
                for edge in edges:
                    link = edge.getLink()
                    hs = feedbackManager.historicalSuccessesFile(discoveredVertex,edge.getDestination(),link,feedt,tk)
                    if hs >= feedbackThreshold:
                        tempPlan.getVertexs()[service["name"]]=discoveredVertex
                        tempPlan.update(edge.getDestination(),edge.getSource(),link["matchedInput"],link["matchedOutput"])
                        tempPlan.addEdge(edge)
                        for domain in service["domains"]:
                            if domain not in tempPlan.getDomains():
                                tempPlan.getDomains().append(domain)
                        addTemp = True
            """If the plan was modified, add it to the list of tempPlans that can meet input requirements or used for next recursion"""
            if addTemp:
                """tempPlans[len(tempPlans)]=copy.deepcopy(tempPlan)
                planChanged = True"""
                calculateMark(tempPlan)
                if tempPlan.getMark() >= functionalThreshold:
                    tempPlans[len(tempPlans)]=copy.deepcopy(tempPlan)
                    planChanged = True
            if (len(tempPlans)) == top:
                break
        if (len(tempPlans)) == top:
            break
            
             
    """Comparison of discovered plans with request inputs"""
    for k1, plan in tempPlans.items():
        initial = plan.getVertexs()["initial"]
        init = False
        remainingVertexs = plan.getRemainingVertexs()
        comparison = hybridMatchmaker2(remainingVertexs, initial, matchingType, similarityThreshold,)        
        if comparison["match"] == True:
            init = True
            edges = comparison["edges"]
            for edge in edges:
                link = edge.getLink()
                hs = feedbackManager.historicalSuccessesFile(initial,edge.getDestination(),link,feedt,tk)
                if hs >= feedbackThreshold:
                    plan.update(edge.getDestination(),edge.getSource(),link["matchedInput"],link["matchedOutput"])
                    plan.addEdge(edge)
        if len(plan.getRemainingVertexs())==0 and init == True: 
            plan.setState(1)
            plans[len(plans)]=copy.deepcopy(plan)
    
    if len(plans)==0 and planChanged:        
        """Recursion for not solved Plans and get solved plans after recursion"""
        solvedPlans = {}
        tempPlans = calculateMarks(tempPlans)
        tempPlans = rankPlans(tempPlans)
        tempPlans = topK(tempPlans,top)
        recursivePlans = backwardPlanningHeuristic(jobId,jsonRequest,tempPlans,matchingType,similarityThreshold,feedbackThreshold,top,functionalThreshold,servicesPerGateway,feedt,tk)
        for k2, rplan in recursivePlans.items():
            tempPlans[len(tempPlans)] = rplan
        for k, plan in tempPlans.items():     
            if plan.getState()==1:    
                plans[len(plans)]=copy.deepcopy(plan)
    
    if len(plans)>0:
        del tempPlans
        plans = calculateMarks(plans)
        plans = rankPlans(plans)
        plans = topK(plans,top)
        return plans
    elif len(tempPlans)>0:
        tempPlans = calculateMarks(tempPlans)
        tempPlans = rankPlans(tempPlans)
        tempPlans = topK(tempPlans,top)
        return tempPlans
    else:
        return plans
    
"""
Backward planning algorithm reducing search space with Mongo DB queries and using ACO to include knowledge about previous discovery processes.
@jsonRequest: Original request in JSON format.
@previousPlans: Collection of plans previously discovered in other gateways.
@matchingType: Matchmaking type:
    5 - Semantic matchmaker (Logic).
    6 - Cosine Similarity (Non-logic).
    7 - Jaccard Index (Non-Logic).
    8 - Semantic matchmaker and Cosine Similarity (Hybrid).
    9 - Semantic matchmaker and Jaccard Index (Hybrid).
@similarityThreshold : Threshold for syntactic matching.
@feedbackThreshold: Threshold to check in discovered edges.
@top : K plans to be choosen.
@functionalThreshold: Threshold to check to add a discovered plan in the list of top K plans.
@servicesPerGateway : Defines with file of services to read in TCHPC experiments.
@return Discovered plans according to functional requirements.
"""
def backwardPlanningHeuristicACO(jobId,jsonRequest,previousPlans,matchingType,similarityThreshold,feedbackThreshold, top, functionalThreshold, servicesPerGateway):
    #print("Backward planning: " + jsonRequest["id"])
    plans = {}
    """ Create new plan or use previous"""
    if(len(previousPlans) == 0):
        plan = PlanGraph(0, {}, [], 0.0, [], [], [])
        initial = Vertex(0, "initial", 1, {}, [], [], jsonRequest["inputs"][:], jsonRequest["domains"][:])
        initial.setState(1);
        plan.getVertexs()[initial.getName()] = initial
        final = Vertex(1, "final", 0, {}, jsonRequest["outputs"][:], jsonRequest["outputs"][:],[], jsonRequest["domains"][:])
        final.setRemainingInputs(jsonRequest["outputs"][:])
        plan.getVertexs()[final.getName()] = final
        plan.setInputs(jsonRequest["inputs"][:])
        plan.setOutputs(jsonRequest["outputs"][:])
        plan.setCurrentInputs(jsonRequest["outputs"][:])
        plan.setState(0)
        for domain in jsonRequest["domains"]:
            if domain not in plan.getDomains():
                plan.getDomains().append(domain)
        previousPlans[len(previousPlans)] = plan
        
    """Graph creation"""
    tempPlans = {}
    planChanged = False
    np = 0
    for kp, plan in previousPlans.items():
        np = np + 1
        requestOutputs = plan.getCurrentInputs()[:]
        requestInputs = plan.getInputs()[:]
        """Defining search space"""
        parametersTypes = getParameterTypes(requestOutputs)
        searchSpace = []
        servicesNames = []
        #searchSpace = services.find({ "outputs.type": {"$in" : parametersTypes } })
        if servicesPerGateway == 2000:
            servicesFile = services2000File
        
        if servicesPerGateway == 4000:
            servicesFile = services4000File
        
        if servicesPerGateway == 6000:
            servicesFile = services6000File
        
        if servicesPerGateway == 8000:
            servicesFile = services8000File
        
        if servicesPerGateway == 10000:
            servicesFile = services10000File
        
        for service in servicesFile:
            add = False
            for parameterType in parametersTypes:
                serviceOutputs = []
                serviceOutputs = service["outputs"]
                for output in serviceOutputs:
                    if output["type"] == parameterType:
                        add = True
                        break
            if add:
                service["name"] = service["name"].replace(".","")
                if service["name"] not in servicesNames :
                    searchSpace.append(service)
                    servicesNames.append(service["name"])
            
        if len(searchSpace) == 0:
            servicesList = servicesFile
            for service in servicesList:
                service["name"] = service["name"].replace(".","")
                if service["name"] not in servicesNames:
                    searchSpace.append(service)
                    servicesNames.append(service["name"])
                          
        """Compare each output of each service with each remaining input on each remaining vertex"""
        for service in searchSpace:
            tempPlan = copy.deepcopy(plan)
            addTemp = False
            discoveredVertex = Vertex(-1, "", 0, {}, [], [], [], [])
            if service["name"] in tempPlan.getVertexs().keys():
                discoveredVertex = tempPlan.getVertexs()[service["name"]]
            else:
                discoveredVertex.setType(2)
                discoveredVertex.setName(service["name"])
                discoveredVertex.setState(0)
                discoveredVertex.setService(service)
                discoveredVertex.setInputs(service["inputs"][:])
                discoveredVertex.setRemainingInputs(service["inputs"][:])
                discoveredVertex.setOutputs(service["outputs"][:])
                discoveredVertex.setDomains(service["domains"][:])
            remainingVertexs = tempPlan.getRemainingVertexs()
            comparison = hybridMatchmaker2(remainingVertexs, discoveredVertex, matchingType, similarityThreshold)        
            if comparison["match"] == True:
                edges = comparison["edges"]
                for edge in edges:
                    link = edge.getLink()
                    hs = feedbackManager.historicalSuccessesFile(discoveredVertex,edge.getDestination(),link)
                    if hs >= feedbackThreshold:
                        tempPlan.getVertexs()[service["name"]]=discoveredVertex
                        tempPlan.update(edge.getDestination(),edge.getSource(),link["matchedInput"],link["matchedOutput"])
                        tempPlan.addEdge(edge)
                        for domain in service["domains"]:
                            if domain not in tempPlan.getDomains():
                                tempPlan.getDomains().append(domain)
                        addTemp = True
            """If the plan was modified, add it to the list of tempPlans that can meet input requirements or used for next recursion"""
            if addTemp:
                """tempPlans[len(tempPlans)]=copy.deepcopy(tempPlan)
                planChanged = True"""
                calculateMark(tempPlan)
                if tempPlan.getMark() >= functionalThreshold:
                    tempPlans[len(tempPlans)]=copy.deepcopy(tempPlan)
                    planChanged = True
            if (len(tempPlans)) == top:
                break
        if (len(tempPlans)) == top:
            break
            
             
    """Comparison of discovered plans with request inputs"""
    for k1, plan in tempPlans.items():
        initial = plan.getVertexs()["initial"]
        init = False
        remainingVertexs = plan.getRemainingVertexs()
        comparison = hybridMatchmaker2(remainingVertexs, initial, matchingType, similarityThreshold,)        
        if comparison["match"] == True:
            init = True
            edges = comparison["edges"]
            for edge in edges:
                link = edge.getLink()
                hs = feedbackManager.historicalSuccessesFile(initial,edge.getDestination(),link)
                if hs >= feedbackThreshold:
                    plan.update(edge.getDestination(),edge.getSource(),link["matchedInput"],link["matchedOutput"])
                    plan.addEdge(edge)
        if len(plan.getRemainingVertexs())==0 and init == True: 
            plan.setState(1)
            plans[len(plans)]=copy.deepcopy(plan)
    
    if len(plans)==0 and planChanged:        
        """Recursion for not solved Plans and get solved plans after recursion"""
        solvedPlans = {}
        tempPlans = calculateMarks(tempPlans)
        tempPlans = rankPlans(tempPlans)
        tempPlans = topK(tempPlans,top)
        recursivePlans = backwardPlanningHeuristic(jobId,jsonRequest,tempPlans,matchingType,similarityThreshold,feedbackThreshold,top,functionalThreshold,servicesPerGateway)
        for k2, rplan in recursivePlans.items():
            tempPlans[len(tempPlans)] = rplan
        for k, plan in tempPlans.items():     
            if plan.getState()==1:    
                plans[len(plans)]=copy.deepcopy(plan)
    
    if len(plans)>0:
        del tempPlans
        plans = calculateMarks(plans)
        plans = rankPlans(plans)
        plans = topK(plans,top)
        return plans
    elif len(tempPlans)>0:
        tempPlans = calculateMarks(tempPlans)
        tempPlans = rankPlans(tempPlans)
        tempPlans = topK(tempPlans,top)
        return tempPlans
    else:
        return plans

"""
Backward planning algorithm using hybrid matchmaker.
@jsonRequest: Original request in JSON format.
@previousPlans: Collection of plans previously discovered in other gateways.
@matchingType: Matchmaking type:
    0 - Semantic matchmaker (Logic).
    1 - Cosine Similarity (Non-logic).
    2 - Jaccard Index (Non-Logic).
    3 - Semantic matchmaker and Cosine Similarity (Hybrid).
    4 - Semantic matchmaker and Jaccard Index (Hybrid).
@simThreshold : Similarity threshold.
@servicesPerGateway : Defines with file of services to read in TCHPC experiments. 
@return List of discovered plans.
"""
def backwardPlanningClassic2(jsonRequest,previousPlans,matchingType,similarityThreshold,planLength,servicesPerGateway):
    #print("Backward planning: " + jsonRequest["id"])
    plans = {}
    """ Create new plan or use previous"""
    if(len(previousPlans) == 0):
        plan = PlanGraph(0, {}, [], 0.0, [], [], [])
        initial = Vertex(0, "initial", 1, {}, [], [], jsonRequest["inputs"][:], jsonRequest["domains"][:])
        initial.setState(1);
        plan.getVertexs()[initial.getName()] = initial
        final = Vertex(1, "final", 0, {}, jsonRequest["outputs"][:], jsonRequest["outputs"][:], [], jsonRequest["domains"][:])
        final.setRemainingInputs(jsonRequest["outputs"][:])
        plan.getVertexs()[final.getName()] = final
        plan.setInputs(jsonRequest["inputs"][:])
        plan.setOutputs(jsonRequest["outputs"][:])
        plan.setCurrentInputs(jsonRequest["outputs"][:])
        plan.setState(0)
        for domain in jsonRequest["domains"]:
            if domain not in plan.getDomains():
                plan.getDomains().append(domain)
        previousPlans[len(previousPlans)] = plan
        
    """Graph creation"""
    for kp, plan in previousPlans.items():
        requestOutputs = plan.getCurrentInputs()
        requestInputs = plan.getInputs()
        """Defining search space"""
        parametersTypes = getParameterTypes(requestOutputs)
        """Defining search space"""
        if servicesPerGateway == 2000:
            searchSpace = services2000File
        
        if servicesPerGateway == 4000:
            searchSpace = services4000File
        
        if servicesPerGateway == 6000:
            searchSpace = services6000File
        
        if servicesPerGateway == 8000:
            searchSpace = services8000File
        
        if servicesPerGateway == 10000:
            searchSpace = services10000File
        
        """Compare each output of each service with each remaining input on each remaining vertex"""
        for service in searchSpace:
            tempPlan = copy.deepcopy(plan)
            addTemp = False
            discoveredVertex = Vertex(-1, "", 0, {}, [], [], [], [])
            if service["name"] in tempPlan.getVertexs().keys():
                discoveredVertex = tempPlan.getVertexs()[service["name"]]
            else:
                discoveredVertex.setType(2)
                discoveredVertex.setName(service["name"])
                discoveredVertex.setState(0)
                discoveredVertex.setService(service)
                discoveredVertex.setInputs(service["inputs"][:])
                discoveredVertex.setRemainingInputs(service["inputs"][:])
                discoveredVertex.setOutputs(service["outputs"][:])
                discoveredVertex.setDomains(service["domains"][:])
            remainingVertexs = tempPlan.getRemainingVertexs()
            comparison = hybridMatchmaker2(remainingVertexs, discoveredVertex, matchingType, similarityThreshold)        
            if comparison["match"] == True:
                edges = comparison["edges"]
                for edge in edges:
                    link = edge.getLink()
                    tempPlan.getVertexs()[service["name"]]=discoveredVertex
                    tempPlan.update(edge.getDestination(),edge.getSource(),link["matchedInput"],link["matchedOutput"])
                    tempPlan.addEdge(edge)
                    for domain in service["domains"]:
                        if domain not in tempPlan.getDomains():
                            plan.getDomains().append(domain)
                    addTemp = True
            if addTemp:
                initial = tempPlan.getVertexs()["initial"]
                init = False
                remainingVertexsTemp = tempPlan.getRemainingVertexs()
                comparison = hybridMatchmaker2(remainingVertexsTemp, initial, matchingType, similarityThreshold)        
                if comparison["match"] == True:
                    init = True
                    edges = comparison["edges"]
                    for edge in edges:
                        link = edge.getLink()
                        tempPlan.update(edge.getDestination(),edge.getSource(),link["matchedInput"],link["matchedOutput"])
                        tempPlan.addEdge(edge)
                if len(tempPlan.getRemainingVertexs()) == 0 and init == True:
                    if len(tempPlan.getVertexs()) - 2 >= 1:
                        tempPlan.setState(1)
                        tempPlan.setServices(len(tempPlan.getVertexs()) - 2)
                        plans[len(plans)] = copy.deepcopy(tempPlan)
                else:
                    if len(tempPlan.getVertexs()) - 2 < planLength:
                        tempPlans = {}
                        tempPlans[len(tempPlans)] = copy.deepcopy(tempPlan)
                        tempPlans = backwardPlanningClassic2(jsonRequest,tempPlans,matchingType,similarityThreshold,planLength,servicesPerGateway) 
                        for k3, p in tempPlans.items():
                            if p.getState() == 1 and len(p.getVertexs()) - 2 > 1:
                                p.setServices(len(p.getVertexs()) - 2)
                                plans[len(plans)] = copy.deepcopy(p)
    return plans
    
"""
Backward planning algorithm reducing search space with Mongo DB queries and using knowledge about previous discovery processes.
@jsonRequest: Original request in JSON format.
@previousPlans: Collection of plans previously discovered in other gateways.
@matchingType: Matchmaking type:
    5 - Semantic matchmaker (Logic).
    6 - Cosine Similarity (Non-logic).
    7 - Jaccard Index (Non-Logic).
    8 - Semantic matchmaker and Cosine Similarity (Hybrid).
    9 - Semantic matchmaker and Jaccard Index (Hybrid).
@similarityThreshold : Threshold for syntactic matching.
@feedbackThreshold: Threshold to check in discovered edges.
@top : K plans to be choosen.
@functionalThreshold: Threshold to check to add a discovered plan in the list of top K plans.
@return Discovered plans according to functional requirements.
"""
def backwardPlanningHeuristic2(jobId,jsonRequest,previousPlans,matchingType,similarityThreshold,feedbackThreshold, top, functionalThreshold,planLength):
    #print("Backward planning: " + jsonRequest["id"])
    plans = {}
    """ Create new plan or use previous"""
    if(len(previousPlans) == 0):
        plan = PlanGraph(0, {}, [], 0.0, [], [], [])
        initial = Vertex(0, "initial", 1, {}, [], [], jsonRequest["inputs"][:], jsonRequest["domains"][:])
        initial.setState(1);
        plan.getVertexs()[initial.getName()] = initial
        final = Vertex(1, "final", 0, {}, jsonRequest["outputs"][:], jsonRequest["outputs"][:],[], jsonRequest["domains"][:])
        final.setRemainingInputs(jsonRequest["outputs"][:])
        plan.getVertexs()[final.getName()] = final
        plan.setInputs(jsonRequest["inputs"][:])
        plan.setOutputs(jsonRequest["outputs"][:])
        plan.setCurrentInputs(jsonRequest["outputs"][:])
        plan.setState(0)
        for domain in jsonRequest["domains"]:
            if domain not in plan.getDomains():
                plan.getDomains().append(domain)
        previousPlans[len(previousPlans)] = plan
        
    """Graph creation"""
    tempPlans = {}
    planChanged = False
    np = 0
    for kp, plan in previousPlans.items():
        np = np + 1
        requestOutputs = plan.getCurrentInputs()[:]
        requestInputs = plan.getInputs()[:]
        """Defining search space"""
        parametersTypes = getParameterTypes(requestOutputs)
        searchSpace = []
        servicesNames = []
        #searchSpace = services.find({ "outputs.type": {"$in" : parametersTypes } })
        for parameterType in parametersTypes:
            servicesList = services.find({"outputs.type": parameterType})
            for service in servicesList:
                service["name"] = service["name"].replace(".","")
                if service["name"] not in servicesNames :
                    searchSpace.append(service)
                    servicesNames.append(service["name"])
            
        if len(searchSpace) == 0:
            servicesList = services.find({"state":True})
            for service in servicesList:
                service["name"] = service["name"].replace(".","")
                if service["name"] not in servicesNames:
                    searchSpace.append(service)
                    servicesNames.append(service["name"])
                          
        """Compare each output of each service with each remaining input on each remaining vertex"""
        for service in searchSpace:
            tempPlan = copy.deepcopy(plan)
            addTemp = False
            discoveredVertex = Vertex(-1, "", 0, {}, [], [], [], [])
            if service["name"] in tempPlan.getVertexs().keys():
                discoveredVertex = tempPlan.getVertexs()[service["name"]]
            else:
                discoveredVertex.setType(2)
                discoveredVertex.setName(service["name"])
                discoveredVertex.setState(0)
                discoveredVertex.setService(service)
                discoveredVertex.setInputs(service["inputs"][:])
                discoveredVertex.setRemainingInputs(service["inputs"][:])
                discoveredVertex.setOutputs(service["outputs"][:])
                discoveredVertex.setDomains(service["domains"][:])
            remainingVertexs = tempPlan.getRemainingVertexs()
            comparison = hybridMatchmaker2(remainingVertexs, discoveredVertex, matchingType, similarityThreshold)        
            if comparison["match"] == True:
                edges = comparison["edges"]
                for edge in edges:
                    link = edge.getLink()
                    hs = feedbackManager.historicalSuccessesFile(discoveredVertex,edge.getDestination(),link)
                    if hs >= feedbackThreshold:
                        tempPlan.getVertexs()[service["name"]]=discoveredVertex
                        tempPlan.update(edge.getDestination(),edge.getSource(),link["matchedInput"],link["matchedOutput"])
                        tempPlan.addEdge(edge)
                        for domain in service["domains"]:
                            if domain not in tempPlan.getDomains():
                                tempPlan.getDomains().append(domain)
                        addTemp = True
            """If the plan was modified, add it to the list of tempPlans that can meet input requirements or used for next recursion"""
            if addTemp:
                """tempPlans[len(tempPlans)]=copy.deepcopy(tempPlan)
                planChanged = True"""
                calculateMark(tempPlan)
                if tempPlan.getMark() >= functionalThreshold:
                    tempPlans[len(tempPlans)]=copy.deepcopy(tempPlan)
                    planChanged = True
            """if (len(tempPlans)) == top:
                break
        if (len(tempPlans)) == top:
            break"""
            
             
    """Comparison of discovered plans with request inputs"""
    for k1, plan in tempPlans.items():
        initial = plan.getVertexs()["initial"]
        init = False
        remainingVertexs = plan.getRemainingVertexs()
        comparison = hybridMatchmaker2(remainingVertexs, initial, matchingType, similarityThreshold,)        
        if comparison["match"] == True:
            init = True
            edges = comparison["edges"]
            for edge in edges:
                link = edge.getLink()
                hs = feedbackManager.historicalSuccessesFile(initial,edge.getDestination(),link)
                if hs >= feedbackThreshold:
                    plan.update(edge.getDestination(),edge.getSource(),link["matchedInput"],link["matchedOutput"])
                    plan.addEdge(edge)
        if len(plan.getRemainingVertexs())==0 and init == True and len(plan.getVertexs()) > planLength: 
            plan.setState(1)
            plans[len(plans)]=copy.deepcopy(plan)
    
    if len(plans)==0 and planChanged:        
        """Recursion for not solved Plans and get solved plans after recursion"""
        solvedPlans = {}
        tempPlans = calculateMarks(tempPlans)
        tempPlans = rankPlans(tempPlans)
        tempPlans = topK(tempPlans,top)
        recursivePlans = backwardPlanningHeuristic2(jobId,jsonRequest,tempPlans,matchingType,similarityThreshold,feedbackThreshold,top,functionalThreshold,planLength)
        for k2, rplan in recursivePlans.items():
            tempPlans[len(tempPlans)] = rplan
        for k, plan in tempPlans.items():     
            if plan.getState()==1:    
                plans[len(plans)]=copy.deepcopy(plan)
    
    if len(plans)>0:
        plansToReturn = {}
        del tempPlans
        for k, plan in plans.items():
            if plan.getState() == 1:
                plansToReturn[len(plansToReturn)] = plan
        plansToReturn = calculateMarks(plansToReturn)
        plansToReturn = rankPlans(plansToReturn)
        plansToReturn = topK(plansToReturn,top)
        return plansToReturn
    elif len(tempPlans)>0:
        plansToReturn = {}
        for k, plan in tempPlans.items():
            if plan.getState() == 1:
                plansToReturn[len(plansToReturn)] = plan
        plansToReturn = calculateMarks(plansToReturn)
        plansToReturn = rankPlans(plansToReturn)
        plansToReturn = topK(plansToReturn,top)
        return plansToReturn
    else:
        return plans

"""
Get super classes and sub classes of a list of parameters.
@parameters: List of parameters to determine superClasses and sub classes.
@return List of super and sub classes.
"""
def getParameterTypes(parameters):
    parameterTypes = []
    for parameter in parameters:
        parameterType = parameter["type"]
        if parameterType not in parameterTypes:
            parameterTypes.append(parameterType)
        ontology = parameterType.split("#")
        if(ontology[0]!="http://surf.mockup.ie/ontology" and ontology[0]!="http://127.0.0.1/ontology/protont.owl" and ontology[0]!="http://127.0.0.1/ontology/protonu.owl" and ontology[0]!="http://127.0.0.1/ontology/protons.owl"):
            try:
                onto = get_ontology(ontology[0])
                onto.load()
                parameterClasses = onto.search(iri = parameterType)
                for parameterClass in parameterClasses:
                    subconcepts = onto.search(subclass_of = parameterClass) 
                    for subconcept in subconcepts:
                        if subconcept not in parameterTypes:
                            parameterTypes.append(subconcept.iri)
                    superconcepts = parameterClass.is_a
                    for superconcept in superconcepts:
                        if superconcept not in parameterTypes and str(type(superconcept)) == "<class 'owlready2.entity.ThingClass'>" :
                            parameterTypes.append(superconcept.iri)
            except:
                print()
    return parameterTypes

"""
Hybrid matchmaker between I/O parameters.
@parameterInput: Input parameter of the vertex to be solved.
@parameterOutput: Output parameter of the service that can solve the input.
@matchingtype: Matchmaking type:
    0 - Semantic matchmaker (Logic).
    1 - Cosine Similarity (Non-logic).
    2 - Jaccard Index (Non-Logic).
    3 - Semantic matchmaker and Cosine Similarity (Hybrid).
    4 - Semantic matchmaker and Jaccard Index (Hybrid).
@simThreshold: Similarity threshold. 
@return Comparison output.
    match - True if match, False if not.
    link - Parameters that creates the link.
    degree - Degree of matching, used to rank the plans.
        4 - Exact semantic matching.
        3 - Plugin semantic matching.
        2 - Subsume semantic matching.
        1 - Syntactic matching.
"""
def hybridMatchmaker(parameterInput, parameterOutput, matchingType, simThreshold):
    comparison = {}
    match = False
    link = {}
    degree = 0.0
    if (matchingType == 0 or matchingType == 3 or matchingType == 4):
        if(exactMatch(parameterInput,parameterOutput)):
            match = True
            link["matchedOutput"]=parameterOutput
            link["matchedInput"]=parameterInput
            degree = 4
        elif(pluginMatch(parameterInput,parameterOutput)):
            match = True
            link["matchedOutput"]=parameterOutput
            link["matchedInput"]=parameterInput
            degree = 3
        elif(subsumeMatch(parameterInput,parameterOutput)):
            match = True
            link["matchedOutput"]=parameterOutput
            link["matchedInput"]=parameterInput
            degree = 2
    if (matchingType == 1 or matchingType == 2 or matchingType == 3 or matchingType == 4):
        if(syntaticMatch(parameterInput,parameterOutput,matchingType,simThreshold)):
            match = True
            link["matchedOutput"]=parameterOutput
            link["matchedInput"]=parameterInput
            degree = 1
    comparison["match"]=match
    comparison["link"]=link
    comparison["degree"]=degree
    return comparison


"""
Hybrid matchmaker between list of I/O parameters.
@remainingInputs: List of input parameters of the plan to be solved.
@serviceOutputs: Output parameters of the service that can solve the inputs.
@matchingtype: Matchmaking type:
    0 - Semantic matchmaker (Logic).
    1 - Cosine Similarity (Non-logic).
    2 - Jaccard Index (Non-Logic).
    3 - Semantic matchmaker and Cosine Similarity (Hybrid).
    4 - Semantic matchmaker and Jaccard Index (Hybrid).
@simThreshold: Similarity threshold. 
@return Comparison output.
    match - True if match, False if not.
    edges - Identified matches
        link - Source and destination of the relation.        
        degree - Degree of matching, used to rank the plans.
            4 - Exact semantic matching.
            3 - Plugin semantic matching.
            2 - Subsume semantic matching.
            1 - Syntactic matching.
"""
def hybridMatchmaker2(remainingVertexs, newVertex, matchingType, simThreshold):
    comparison = {}
    match = False
    edges = []
    
    for k, remainingVertex in remainingVertexs.items():
        vertex = copy.deepcopy(remainingVertex)
        if vertex.getName() != newVertex.getName():
            remainingInputs = vertex.getRemainingInputs()[:]
            for input in remainingInputs:
                input["state"] = "notSolved"
            newVertexOutputs = newVertex.getOutputs()[:]
            for output in newVertexOutputs:
                output["state"] = "notUsed"
            if (matchingType == 0 or matchingType == 3 or matchingType == 4): 
                for input in remainingInputs:
                    if input["state"] == "notSolved":
                        for output in newVertexOutputs:
                            if exactMatch(input, output) and input["state"] == "notSolved" and output["state"] == "notUsed":
                                input["state"] = "solved"
                                output["state"] = "used"
                                match = True
                                link = {}
                                link["matchedOutput"]=output
                                link["matchedInput"]=input
                                edge = Edge(newVertex, remainingVertex, link, 4)
                                edges.append(edge)
                for input in remainingInputs:
                    if input["state"] == "notSolved":
                        newVertexOutputs = newVertex.getOutputs()
                        for output in newVertexOutputs:
                            if pluginMatch(input, output) and input["state"] == "notSolved" and output["state"] == "notUsed":
                                input["state"] = "solved"
                                output["state"] = "used"
                                match = True
                                link = {}
                                link["matchedOutput"]=output
                                link["matchedInput"]=input
                                edge = Edge(newVertex, remainingVertex, link, 3)
                                edges.append(edge)                
                for input in remainingInputs:
                    if input["state"] == "notSolved":
                        newVertexOutputs = newVertex.getOutputs()
                        for output in newVertexOutputs:
                            if subsumeMatch(input, output) and input["state"] == "notSolved" and output["state"] == "notUsed":
                                input["state"] = "solved"
                                output["state"] = "used"
                                match = True
                                link = {}
                                link["matchedOutput"]=output
                                link["matchedInput"]=input
                                edge = Edge(newVertex, remainingVertex, link, 2)
                                edges.append(edge)
            if (matchingType == 1 or matchingType == 2 or matchingType == 3 or matchingType == 4):
                for input in remainingInputs:
                    if input["state"] == "notSolved":
                        newVertexOutputs = newVertex.getOutputs()
                        for output in newVertexOutputs:
                            if syntaticMatch(input, output,matchingType,simThreshold) and input["state"] == "notSolved" and output["state"] == "notUsed":
                                input["state"] = "solved"
                                output["state"] = "used"
                                match = True
                                link = {}
                                link["matchedOutput"]=output
                                link["matchedInput"]=input
                                edge = Edge(newVertex, remainingVertex, link, 1)
                                edges.append(edge)
    comparison["match"]=match
    comparison["edges"]=edges
    return comparison

"""
Exact match between two I/O parameters.
@parameterRequest: Request parameter.
@parameterService: Service parameter.
@return True if @parameterRequest EQUIVALENT TO @parameterService.
"""
def exactMatch(parameterRequest, parameterService):
    match = False
    if(parameterRequest["type"]==parameterService["type"]):
        match = True
    return match

"""
Plugin match between two I/O parameters.
@parameterRequest: Request parameter.
@parameterService: Service parameter.
@return True if @parameterRequest SUBCONCEPT OF @parameterService.
"""
def pluginMatch(parameterRequest, parameterService):
    match = False
    serviceType = parameterService["type"]
    parameterType = parameterRequest["type"]
    ontology = serviceType.split("#")
    if(ontology[0]=="http://surf.mockup.ie/ontology" or ontology[0]=="http://127.0.0.1/ontology/protont.owl" or ontology[0]=="http://127.0.0.1/ontology/protonu.owl" or ontology[0]=="http://127.0.0.1/ontology/protons.owl"):
        return False
    try:
        onto = get_ontology(ontology[0])
        onto.load()
        serviceClasses = onto.search(iri = serviceType)
        parameterClasses = onto.search(iri = parameterType)
        if(len(parameterClasses)==0):
            return False
        else:
            subconcepts = onto.search(subclass_of = serviceClasses)
            for parameterClass in parameterClasses:
                if(parameterClass in subconcepts):
                    return True
        return False
    except:
        return False
    
"""
Subsume match between two I/O parameters.
@parameterRequest: Request parameter.
@parameterService: Service parameter.
@return True if @parameterRequest SUPERCONCEPT OF @parameterService.
"""
def subsumeMatch(parameterRequest, parameterService):
    match = False
    serviceType = parameterService["type"]
    parameterType = parameterRequest["type"]
    ontology = serviceType.split("#")
    onto = get_ontology(ontology[0])
    if(ontology[0]=="http://surf.mockup.ie/ontology" or ontology[0]=="http://127.0.0.1/ontology/protont.owl" or ontology[0]=="http://127.0.0.1/ontology/protonu.owl" or ontology[0]=="http://127.0.0.1/ontology/protons.owl"):
        return False
    try:
        onto.load()
        serviceClasses = onto.search(iri = serviceType)
        parameterClasses = onto.search(iri = parameterType)
        if(not parameterClasses):
            return False
        else:
            subconcepts = onto.search(subclass_of = parameterClasses)
            for serviceClass in serviceClasses:
                if(serviceClass in subconcepts):
                    return True
                else:
                    return False
    except:
        return False

"""
Syntactic matchmaker between I/O parameters.
@parametersRequest: List of request parameters.
@parametersService: List of service parameters.
@matchingtype: Matchmaking type:
    1 or 3 - Cosine Similarity (Non-logic).
    2 or 4 - Jaccard Index (Non-Logic).
@simThreshold: Similarity threshold. 
@return True if similarity(@parameterRequest,@parameterService)==1 or similarity(@parameterRequest,@parameterService)>=distance.
"""
def syntaticMatch(parameterRequest, parameterService, matchingType, simThreshold):
    requestDescriptionAll = parameterRequest["desc"].split()
    serviceDescriptionAll = parameterService["desc"].split()
    
    requestDescription = []
    serviceDescription = []
    
    for word in requestDescriptionAll:
        if word.lower() not in to_remove:
            requestDescription.append(word)
    
    for word in serviceDescriptionAll:
        if word.lower() not in to_remove:
            serviceDescription.append(word)
    
    if(matchingType == 1 or matchingType == 3):
        similarity = cosineSimilarity(requestDescription,serviceDescription)
        if similarity == 1 or similarity >= simThreshold:
            return True
        else:
            return False
    
    if(matchingType == 2 or matchingType == 4):
        similarity = jaccardIndex(requestDescription,serviceDescription)
        if similarity == 1 or similarity >= simThreshold:
            return True
        else:
            return False

"""
Cosine similarity between two vectors.
@requestDescription: Vector of the words of the request parameter.
@serviceDescription: Vector of the words of the service parameter.
@return Cosine similarity between vectors.
"""
def cosineSimilarity(requestDescription, serviceDescription):
    words = []
    for word in requestDescription:
        words.append(word)
        for word in serviceDescription:
            if word not in words:
                words.append(word)
    
    vectorRequest = []
    for word in words:
        if word in requestDescription:
            vectorRequest.append(1)
        else:
            vectorRequest.append(0) 
    vectorService = []
    for word in words:
        if word in serviceDescription:
            vectorService.append(1)
        else:
            vectorService.append(0)
    
    cosineSimilarity = 0.0
    dotProduct = 0
    magVectorRequest = 0.0
    magVectorService = 0.0
    i = 0
    while i < len(vectorRequest):
         dotProduct = dotProduct + (vectorRequest[i]*vectorService[i])
         magVectorRequest = magVectorRequest + (vectorRequest[i]*vectorRequest[i])
         magVectorService = magVectorService + (vectorService[i]*vectorService[i])
         i = i + 1
    magVectorRequest = math.sqrt(magVectorRequest)
    magVectorService = math.sqrt(magVectorService)
    cosineSimilarity = dotProduct/(magVectorRequest*magVectorService)
    
    return cosineSimilarity
    
"""
Jaccard index between two vectors.
@requestDescription: Vector of the words of the request parameter.
@serviceDescription: Vector of the words of the service parameter.
@return Jaccard index between vectors.
"""
def jaccardIndex(vectorRequest, vectorService):
    jaccardIndex = 0
    
    intersection = []
    for word in vectorRequest:
        if word in vectorService:
            if word not in intersection:
                intersection.append(word)
    
    union = []
    for word in vectorRequest:
        if word not in union:
            union.append(word)
    for word in vectorService:
        if word not in union:
            union.append(word)
                    
    jaccardIndex = len(intersection)/len(union)
    return jaccardIndex

"""
Compute marks for the discovered plans.
@plans: List of plans without marks.
@return List of plans with marks.
"""
def calculateMarks(plans):
    for k, plan in plans.items():
        mark = 0.0
        totalPossible = len(plan.getEdges()) * 4
        totalActual = 0
        for edge in plan.getEdges():
            totalActual = totalActual + edge.getDegree()
        mark = totalActual/totalPossible
        plan.setMark(mark)
        plans[k] = plan
    return plans

"""
Compute mark for a discovered plans.
@plan: Plans without mark.
@return Plans with marks.
"""
def calculateMark(plan):
    mark = 0.0
    totalPossible = len(plan.getEdges()) * 4
    totalActual = 0
    for edge in plan.getEdges():
        totalActual = totalActual + edge.getDegree()
    mark = totalActual/totalPossible
    plan.setMark(mark)
    return plan
        
    
"""
Rank plans according to mark.
@plans: List of plans to be ranked.
@return List of ranked plans.
"""    
def rankPlans(plans):
    rankedPlans = {}
    for k1, plan in plans.items():
        if len(rankedPlans)==0:
            rankedPlans[len(rankedPlans)] = plan
        else:
            i = 0
            while i < len(rankedPlans):
                ranked = rankedPlans[i]
                if plan.getMark()>ranked.getMark():
                    newK = len(rankedPlans)
                    while newK > i:
                        rankedPlans[newK]=rankedPlans[newK- 1]
                        newK = newK - 1
                    rankedPlans[i]=plan
                    break
                i = i + 1
            if i == len(rankedPlans):
                rankedPlans[len(rankedPlans)] = plan
    return rankedPlans

"""
Get top K plans.
@plans: List of plans to be ranked.
@return List of ranked plans.
"""    
def topK(plans, k):
    topK = {}
    i = 0
    if k < len(plans):
        i = 0
        while i < k:
            topK[i] = plans[i]
            i = i + 1
        return topK
    else:
        return plans  

"""
Convert a list of graphs to a list of JSON plans.
@plans: List of graphs.
@return List of plans.
""" 
def graphsToJsonPlans(graphs,jobId):
    plans = []
    for k, graph in graphs["plans"].items():
        plan = {}
        plan["jobId"] = jobId
        plan["planId"] = k
        plan["state"] = graph.getState()
        plan["mark"] = graph.getMark()
        plan["services"] = graph.getServices()
        
        domains = []
        for domain in graph.getDomains():
            domains.append(domain)
        plan["domains"] = domains
        
        inputs = []
        for input in graph.getInputs()[:]:
            inputs.append(input)
        plan["inputs"] = inputs
        
        outputs = []
        for output in graph.getOutputs()[:]:
            outputs.append(output)
        plan["outputs"] = outputs
        
        vertices = {}
        for k, vertex in graph.getVertexs().items():
            
            if vertex.getName() == "initial":
                v = {}
                v["name"] = vertex.getName()
                v["outputs"] = vertex.getOutputs()
                vertices[k]=v
            elif vertex.getName() == "final":
                v = {}
                v["name"] = vertex.getName()
                v["inputs"] = vertex.getInputs()
                vertices[k]=v
            else:
                v = vertex.getService()  
                vertices[k]=v
        plan["vertices"] = vertices
        
        edges = []
        for edge in graph.getEdges():
            ed = {}
            ed["source"] = edge.getSource().getName()
            ed["destination"] = edge.getDestination().getName()
            ed["degree"] = edge.getDegree()
            link = {}
            link["matchedOutput"] = edge.getLink()["matchedOutput"]
            link["matchedInput"]= edge.getLink()["matchedInput"]
            ed["link"] = link
            edges.append(ed)
        plan["edges"] = edges
        plans.append(plan)
    return plans

"""
Plan to JSON Format.
@plan: Plan to be converted.
@return Plan in JSON format.
"""
def planToJson(plan):
        p = {}
        p["state"] = plan.getState()
        domains = []
        for domain in plan.getDomains():
            domains.append(domain)
        p["domains"] = domains
        inputs = []
        for input in plan.getInputs():
            inputs.append(input)
        p["inputs"] = inputs
        outputs = []
        for output in plan.getOutputs():
            outputs.append(output)
        p["outputs"] = outputs
        steps = []
        for k, step in plan.getSteps().items():
            steps.append(stepToJson(step))
        p["steps"] = steps
        return p

"""
Step to JSON Format.
@step: Step to be converted.
@return Step in JSON format.
"""
def stepToJson(step):
    s = {}
    s["type"] = step.getType()
    s["order"] = step.getOrder()
    inputs = []
    for input in step.getInputs():
        inputs.append(input)
    s["inputs"] = inputs
    outputs = []
    for output in step.getOutputs():
        outputs.append(output)
    s["outputs"] = outputs
    if(step.getType() == "SimpleStep"):
        serviceDescription = step.getServiceDescription()
        if("_id" in serviceDescription):
            del serviceDescription["_id"]
        s["serviceDescription"] = serviceDescription
    elif (step.getType() == "SequentialStep"):
        substeps = []
        for k, substep in step.getSteps().items():
            substeps.append(stepToJson(substep))
        s["steps"] = substeps
    elif (step.getType() == "ParallelStep"):
        substeps = []
        for k, substep in step.getSteps().items():
            substeps.append(stepToJson(substep))
        s["steps"] = substeps
    return s

"""
Conversation-based approach to discover composed services.
@jsonRequest: Original request as a set of tasks.
@matchingType: Matchmaking type:
    0 - Semantic matchmaker (Logic).
    1 - Cosine Similarity (Non-logic).
    2 - Jaccard Index (Non-Logic).
    3 - Semantic matchmaker and Cosine Similarity (Hybrid).
    4 - Semantic matchmaker and Jaccard Index (Hybrid).
@similarityThreshold : Threshold for syntactic matching.
@servicesPerGateway : Defines with file of services to read in TCHPC experiments.
@return Discovered plans according to functional requirements.
"""
def conversationApproach(jsonRequest,matchingtype,similarityThreshold,servicesPerGateway):
    requestTasks = jsonRequest["tasks"]
    for task in requestTasks:
        servicesTask = task["services"]
        if len(servicesTask) == 0:
            taskInputs = task["inputs"][:]
            taskOutputs = task["outputs"][:]
            """Defining search space"""
            searchSpace = []
            if servicesPerGateway == 2000:
                searchSpace = services2000File
                        
            if servicesPerGateway == 4000:
                searchSpace = services4000File
        
            if servicesPerGateway == 6000:
                searchSpace = services6000File
        
            if servicesPerGateway == 8000:
                searchSpace = services8000File
        
            if servicesPerGateway == 10000:
                searchSpace = services10000File
            
            for service in searchSpace:
                serviceInputs = service["inputs"][:]
                serviceOutputs = service["outputs"][:]
                if conversationMatch(serviceInputs,taskInputs,serviceOutputs,taskOutputs,matchingtype,similarityThreshold):
                    servicesTask.append(service)
            task["services"] = servicesTask
    return jsonRequest
        
"""
Matching method between a task and a service.
@serviceInputs: Service inputs.
@taskInputs: Task inputs.
@serviceOutputs: Service outputs.
@taskOutputs: Task outputs.
@matchingtype: Matchmaking type:
    0 - Semantic matchmaker (Logic).
    1 - Cosine Similarity (Non-logic).
    2 - Jaccard Index (Non-Logic).
    3 - Semantic matchmaker and Cosine Similarity (Hybrid).
    4 - Semantic matchmaker and Jaccard Index (Hybrid).
@simThreshold: Similarity threshold. 
@return True if match.
"""                
def conversationMatch(serviceInputs,taskInputs,serviceOutputs,taskOutputs,matchingType,simThreshold):
    res = False
    inputsMatch = True
    outputsMatch = True
    for serviceInput in serviceInputs:
        for taskInput in taskInputs:
            taskInput["solved"] = False
            if matchingType==0:
                if exactMatch(serviceInput, taskInput) or pluginMatch(serviceInput, taskInput) or subsumeMatch(serviceInput, taskInput):
                    taskInput["solved"] = True
        
            if matchingType==1 or matchingType == 2:
                if syntaticMatch(serviceInput, taskInput,matchingType,simThreshold):
                    taskInput["solved"] = True
        
            if matchingType==3 or matchingType == 4:
                if exactMatch(serviceInput, taskInput) or pluginMatch(serviceInput, taskInput) or subsumeMatch(serviceInput, taskInput) or syntaticMatch(serviceInput, taskInput,matchingType,simThreshold):
                    taskInput["solved"] = True
    
    for taskInput in taskInputs:
        if not taskInput["solved"]:
            inputsMatch = False
            break
    
                
    if inputsMatch:
        for serviceOutput in serviceOutputs:
            for taskOutput in taskOutputs:
                taskOutput["solved"] = False
                if matchingType==0:
                    if exactMatch(serviceOutput, taskOutput) or pluginMatch(serviceOutput, taskOutput) or subsumeMatch(serviceOutput, taskOutput):
                        taskOutput["solved"] = True
        
                if matchingType==1 or matchingType == 2:
                    if syntaticMatch(serviceOutput, taskOutput,matchingType,simThreshold):
                        taskOutput["solved"] = True
        
                if matchingType==3 or matchingType == 4:
                    if exactMatch(serviceOutput, taskOutput) or pluginMatch(serviceOutput, taskOutput) or subsumeMatch(serviceOutput, taskOutput) or syntaticMatch(serviceOutput, taskOutput,matchingType,simThreshold):
                        taskOutput["solved"] = True
                
        for taskOutput in taskOutputs:
            if not taskOutput["solved"]:
                outputsMatch = False
                break
    
    if inputsMatch and outputsMatch:
        res = True
    else:
        res = False
    return res
                
