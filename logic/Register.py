"""
Service Registration
This project implements a registration component that registers services in a mongoDB.

Author: Christian Cabrera
"""

import pymongo
import os
import json

from random import choice
from pymongo import MongoClient

def serviceRegistration(servicesPerGateway):
    client = MongoClient("localhost", 27017)
    db = client.surf
    services = db.services
    files = os.listdir("../../Data/services-dataset/all/")
    j = 0
    for serviceFile in files:
        try:
            service = "{}"
            with open("../../Data/services-dataset/all/" + serviceFile) as dataFile:    
                service = json.load(dataFile)
            if "domains" in service.keys() and  len(service["domains"]) > 0:
                service["mockup"] = 0
                service_id = services.insert_one(service).inserted_id
                print("Service " + str(j+1) +" of "+ str(servicesPerGateway) + " Service Inserted ID: " + str(service_id))
                j = j + 1
        except Exception:
             print ("Exception registering service")
        if j >= servicesPerGateway:
            break
    
    while j<servicesPerGateway:
        try:
            serviceFile = choice(files)
            service = "{}"
            with open("../../Data/services-dataset/all/" + serviceFile) as dataFile:    
                service = json.load(dataFile)
            if "domains" in service.keys() and  len(service["domains"]) > 0:
                outputs = []
                output = {}
                output["type"] = "http://surf.mockup.ie/mockup#"
                output["name"] = "_MOCKUP"
                output["desc"] = "This is a mockup output"
                outputs.append(output)
                service["outputs"] = outputs
                
                inputs = []
                input = {}
                input["type"] = "http://surf.mockup.ie/ontology#mockup"
                input["name"] = "_MOCKUP"
                input["desc"] = "This is a mockup input"
                inputs.append(input)
                service["inputs"] = inputs
                service["mockup"] = 1
                
                service_id = services.insert_one(service).inserted_id
                print("Service " + str(j+1) +" of "+ str(servicesPerGateway) + " Service Inserted ID: " + str(service_id))
                j = j + 1
        except Exception:
             print ("Exception registering service")

def removeAllServices():
    removed = 0
    client = MongoClient("localhost", 27017)
    db = client.surf
    services = db.services
    graphs = db.graphs
    edges = db.edges
    try:
        result = services.delete_many({})
        print ("Services removed")
        result = graphs.delete_many({})
        print ("Graphs removed")
        result = edges.delete_many({})
        print ("Edges removed")
    except Exception:
        print ("Exception removing data")
        raise

def serviceRegistrationAll():
    client = MongoClient("localhost", 27017)
    db = client.surf
    services = db.services
    files = os.listdir("../../Data/services-dataset/json-services/all/")
    servicesPerGateway = len(files)
    j = 0
    for serviceFile in files:
        try:
            service = "{}"
            with open("../../Data/services-dataset/json-services/all/" + serviceFile) as dataFile:    
                service = json.load(dataFile)
            if "domains" in service.keys() and  len(service["domains"]) > 0:
                service["mockup"] = 0
                service_id = services.insert_one(service).inserted_id
                print("Service " + str(j+1) +" of "+ str(servicesPerGateway) + " Service Inserted ID: " + str(service_id))
                j = j + 1
        except Exception:
             print ("Exception registering service")

def serviceRegistrationAllQoS():
    client = MongoClient("localhost", 27017)
    db = client.surf
    services = db.services
    files = os.listdir("../../Data/services-dataset/json-services/all-qos/")
    servicesPerGateway = len(files)
    j = 0
    for serviceFile in files:
        try:
            service = "{}"
            with open("../../Data/services-dataset/json-services/all-qos/" + serviceFile) as dataFile:    
                service = json.load(dataFile)
            if "domains" in service.keys() and  len(service["domains"]) > 0:
                service["mockup"] = 0
                service_id = services.insert_one(service).inserted_id
                print("Service " + str(j+1) +" of "+ str(servicesPerGateway) + " Service Inserted ID: " + str(service_id))
                j = j + 1
        except Exception:
             print ("Exception registering service")
        
def removeServices(amount):
    removed = 0
    client = MongoClient("localhost", 27017)
    db = client.surf
    services = db.services
    while removed < amount:
        try:
            result = services.delete_one({"mockup": 1})
            removed = removed + 1
            print ("Service " + str(removed) +" removed out of " + str(amount))
        except Exception:
            print ("Exception removing service")
            
def createServicesFile(servicesPerGateway,feedt,tk):
    services = []
    files = os.listdir("../../Data/services-dataset/all/")
    j = 0
    for serviceFile in files:
        try:
            service = "{}"
            with open("../../Data/services-dataset/all/" + serviceFile) as dataFile:    
                service = json.load(dataFile)
            if "domains" in service.keys() and  len(service["domains"]) > 0:
                service["mockup"] = 0
                services.append(service)
                #print("Service " + str(j+1) +" of "+ str(servicesPerGateway) + " Service Inserted ID: " + str(service_id))
                j = j + 1
        except Exception:
             print ("Exception registering service")
    
    while j<servicesPerGateway:
        try:
            serviceFile = choice(files)
            service = "{}"
            with open("../../Data/services-dataset/all/" + serviceFile) as dataFile:    
                service = json.load(dataFile)
            if "domains" in service.keys() and  len(service["domains"]) > 0:
                outputs = []
                output = {}
                output["type"] = "http://surf.mockup.ie/mockup#"
                output["name"] = "_MOCKUP"
                output["desc"] = "This is a mockup output"
                outputs.append(output)
                service["outputs"] = outputs
                
                inputs = []
                input = {}
                input["type"] = "http://surf.mockup.ie/ontology#mockup"
                input["name"] = "_MOCKUP"
                input["desc"] = "This is a mockup input"
                inputs.append(input)
                service["inputs"] = inputs
                service["mockup"] = 1
                services.append(service)
                #print("Service " + str(j+1) +" of "+ str(servicesPerGateway) + " Service Inserted ID: " + str(service_id))
                j = j + 1
        except Exception:
            print ("Exception registering service")
            raise
    with open("../../Data/services-dataset/services-"+str(servicesPerGateway)+".json", "w") as outfile:
        json.dump(services, outfile, indent=3)
        
def cleanServicesFile(servicesPerGateway,approach):
    empty = []
    with open("../../Data/services-dataset/services.json", "w") as outfile:
        json.dump(empty, outfile, indent=3)
        
def cleanHistoricalFiles(feedt,tk):
    empty = []
    with open("../../Data/services-dataset/graphPlans_"+str(feedt)+"_"+str(tk)+".json", "w") as outfile:
        json.dump(empty, outfile, indent=3)
    with open("../../Data/services-dataset/edges_"+str(feedt)+"_"+str(tk)+".json", "w") as outfile:
        json.dump(empty, outfile, indent=3)                              
                    
                    
                    
                    
                    
                    
                    