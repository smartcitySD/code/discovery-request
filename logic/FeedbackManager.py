"""
Feedback Manager class
This class implements the methods to manage the feedback from users.

Author: Christian Cabrera
"""

import json
import time
import os
import math

from random import choice
from pprint import pprint
from pymongo import MongoClient
from reportlab.pdfbase.pdfdoc import Destination

"""
Store discovered plans.
@graphsPlans: Plans to store.
""" 
def storePlans(graphsPlans,jobId):
    """Mongo DB"""
    client = MongoClient('localhost', 27017)
    db = client.surf
    plans = graphsPlans["plans"]
    for k, graphPlan in plans.items():
        toStore = {}
        
        toStore["jobId"] = jobId
        toStore["planId"] = k
        toStore["state"] = graphPlan.getState()
        toStore["mark"] = graphPlan.getMark()
        
        domains = []
        for domain in graphPlan.getDomains():
            domains.append(domain)
        toStore["domains"] = domains
        
        inputs = []
        for input in graphPlan.getInputs()[:]:
            if "state" in input.keys():
                del input["state"]
            inputs.append(input)
        toStore["inputs"] = inputs
        
        outputs = []
        for output in graphPlan.getOutputs()[:]:
            if "state" in output.keys():
                del output["state"]
            outputs.append(output)
        toStore["outputs"] = outputs
        
        vertices = {}
        for k, vertex in graphPlan.getVertexs().items():
            
            if vertex.getName() == "initial":
                v = {}
                v["name"] = vertex.getName()
                v["outputs"] = vertex.getOutputs()
                v["domains"] = vertex.getDomains()
                vertices[k]=v
            elif vertex.getName() == "final":
                v = {}
                v["name"] = vertex.getName()
                v["inputs"] = vertex.getInputs()
                v["domains"] = vertex.getDomains()
                vertices[k]=v
            else:
                v = vertex.getService()  
                del v["_id"]
                vertices[k]=v
        toStore["vertices"] = vertices
        
        edges = []
        for edge in graphPlan.getEdges():
            e = {}
            l = edge.getLink()
            e["source"]=edge.getSource().getName()
            e["matchedOutput"] = l["matchedOutput"]["type"]
            sourceDomains = []
            for domain in edge.getSource().getDomains():
                sourceDomains.append(domain["url"])
            e["sourceDomains"] = sourceDomains
            
            e["destination"]=edge.getDestination().getName()
            e["matchedInput"] = l["matchedInput"]["type"]
            destinationDomains = []
            for domain in edge.getDestination().getDomains():
                destinationDomains.append(domain["url"])
            e["destinationDomains"] = destinationDomains
            e["degree"] = edge.getDegree()
            
            es = db.edges.find({"source":e["source"], "matchedOutput":e["matchedOutput"],"sourceDomains":e["sourceDomains"],"destination":e["destination"],"matchedInput":e["matchedInput"],"destinationDomains":e["destinationDomains"]})
            if es.count() == 0:
                db.edges.update({
                        "source":e["source"],"matchedOutput":e["matchedOutput"],"sourceDomains":e["sourceDomains"],"destination":e["destination"],"matchedInput":e["matchedInput"],"destinationDomains":e["destinationDomains"]
                        },{
                        "$set":{"numberOfDiscoveredEdges" : 1,"degree":e["degree"], "successEdges" : 0, "nonSuccessEdges":0}
                        }, upsert=True)
            else:
                db.edges.update({
                        "source":e["source"],"matchedOutput":e["matchedOutput"],"sourceDomains":e["sourceDomains"],"destination":e["destination"],"matchedInput":e["matchedInput"],"destinationDomains":e["destinationDomains"]
                        },{
                        "$inc":{"numberOfDiscoveredEdges" : 1}
                        }, upsert=False)
            ed = {}
            ed["source"] = edge.getSource().getName()
            ed["destination"] = edge.getDestination().getName()
            ed["degree"] = edge.getDegree()
            link = {}
            matchedOutput = l["matchedOutput"]
            if "state" in matchedOutput.keys():
                del matchedOutput["state"] 
            link["matchedOutput"] = matchedOutput
            matchedInput = l["matchedInput"]
            if "state" in matchedInput.keys():
                del matchedInput["state"] 
            link["matchedInput"]= matchedInput
            ed["link"] = link
            edges.append(ed)
        toStore["edges"] = edges
        
        result = db.graphs.insert_one(toStore)       
    client.close()    

"""
Update Feedback.
@jobId: Job Id of the feedback.
@planId: Discovered plan that receives the feedback.
@feedback: Feedback value:
    0 - Unsuccess.
    1 - Success.
"""
def updateFeedback(jobId,planId,feedback):
    """Mongo DB"""
    client = MongoClient('localhost', 27017)
    db = client.surf
    plans = db.graphs.find({"jobId":jobId,"planId":planId})
    for plan in plans:
        for edge in plan["edges"]:
            e = {}
            l = edge["link"]
            e["matchedOutput"] = l["matchedOutput"]["type"]
            
            vertices = plan["vertices"]
            
            source = vertices[edge["source"]]
            e["source"]=edge["source"]
            sourceDomains = []
            for domain in source["domains"]:
                sourceDomains.append(domain["url"])
            e["sourceDomains"] = sourceDomains
            
            
            e["matchedInput"] = l["matchedInput"]["type"]
            destinationDomains = []
            destination = vertices[edge["destination"]]
            e["destination"]=edge["destination"]
            for domain in destination["domains"]:
                destinationDomains.append(domain["url"])
            e["destinationDomains"] = destinationDomains
            
            if feedback == 0:
                db.edges.update({
                        "source":e["source"],"matchedOutput":e["matchedOutput"],"sourceDomains":e["sourceDomains"],"destination":e["destination"],"matchedInput":e["matchedInput"],"destinationDomains":e["destinationDomains"]
                        },{
                        "$inc":{"nonSuccessEdges" : 1}
                        }, upsert=False)
            if feedback == 1:
                db.edges.update({
                        "source":e["source"],"matchedOutput":e["matchedOutput"],"sourceDomains":e["sourceDomains"],"destination":e["destination"],"matchedInput":e["matchedInput"],"destinationDomains":e["destinationDomains"]
                        },{
                        "$inc":{"successEdges" : 1}
                        }, upsert=False)
    client.close()
    
"""
Historical Successes.
@service: Relation source
@vertex: Relation destination
@link: Edge to check in database.
@return: Historical Success Rate
"""
def historicalSuccesses(service,vertex,link):
    """Mongo DB"""
    client = MongoClient('localhost', 27017)
    db = client.surf
    
    hsr = 0.0
    e = {}
    e["source"] = service.getName()
    e["matchedOutput"] = link["matchedOutput"]["type"]
    sourceDomains = []
    for domain in service.getDomains():
        sourceDomains.append(domain["url"])
    e["sourceDomains"] = sourceDomains
            
    e["destination"] = vertex.getName()
    e["matchedInput"] = link["matchedInput"]["type"]
    destinationDomains = []
    for domain in vertex.getDomains():
        destinationDomains.append(domain["url"])
    e["destinationDomains"] = destinationDomains
    
    edge = db.edges.find_one({"source":e["source"],"matchedOutput":e["matchedOutput"],"sourceDomains":e["sourceDomains"],"destination":e["destination"],"matchedInput":e["matchedInput"],"destinationDomains":e["destinationDomains"]})
    client.close()
    if edge:
        hsr = edge["successEdges"]/edge["numberOfDiscoveredEdges"]
    else:
        hsr = 1.0
    return hsr

"""
Clean Mongo for new experiment.
"""
def clean():
    """Mongo DB"""
    print("Removing Graphs and Edges")
    client = MongoClient('localhost', 27017)
    db = client.surf
    db.graphs.delete_many({})
    db.edges.delete_many({})
    
def updateServiceCollection(servicesPerGateway,numberOfGateway):
    """Mongo DB"""
    print("Removing Services")
    client = MongoClient('localhost', 27017)
    db = client.surf
    db.services.delete_many({})
    services = db.services
    files = os.listdir("../../Data/services-dataset/"+str(numberOfGateway)+"/")
    j = 0
    for serviceFile in files:
        try:
            service = "{}"
            with open("../../Data/services-dataset/"+str(numberOfGateway)+"/" + serviceFile) as dataFile:    
                service = json.load(dataFile)
            if "domains" in service.keys() and  len(service["domains"]) > 0:
                service["mockup"] = 0
                service_id = services.insert_one(service).inserted_id
                #print("Service " + str(j+1) +" of "+ str(servicesPerGateway) + " Service Inserted ID: " + str(service_id))
                j = j + 1
        except Exception:
             print ("Exception registering service")
    
    while j<servicesPerGateway:
        try:
            serviceFile = choice(files)
            service = "{}"
            with open("../../Data/services-dataset/"+str(numberOfGateway)+"/" + serviceFile) as dataFile:    
                service = json.load(dataFile)
            if "domains" in service.keys() and  len(service["domains"]) > 0:
                outputs = []
                output = {}
                output["type"] = "http://surf.mockup.ie/mockup#"
                output["name"] = "_MOCKUP"
                output["desc"] = "This is a mockup output"
                outputs.append(output)
                service["outputs"] = outputs
                
                inputs = []
                input = {}
                input["type"] = "http://surf.mockup.ie/ontology#mockup"
                input["name"] = "_MOCKUP"
                input["desc"] = "This is a mockup input"
                inputs.append(input)
                service["inputs"] = inputs
                service["mockup"] = 1
                
                service_id = services.insert_one(service).inserted_id
                #print("Service " + str(j+1) +" of "+ str(servicesPerGateway) + " Service Inserted ID: " + str(service_id))
                j = j + 1
        except Exception:
             print ("Exception registering service")
             
"""
Historical Successes from file.
@service: Relation source
@vertex: Relation destination
@link: Edge to check in database.
@return: Historical Success Rate
"""
def historicalSuccessesFile(service,vertex,link,feedt,tk):
    edges = []
    with open("../../Data/services-dataset/edges_"+str(feedt)+"_"+str(tk)+".json") as dataFile:    
        edges = json.load(dataFile)
    hsr = 0.0
    
    e = {}
    e["source"] = service.getName()
    e["matchedOutput"] = link["matchedOutput"]["type"]
    sourceDomains = []
    for domain in service.getDomains():
        sourceDomains.append(domain["url"])
    e["sourceDomains"] = sourceDomains
            
    e["destination"] = vertex.getName()
    e["matchedInput"] = link["matchedInput"]["type"]
    destinationDomains = []
    for domain in vertex.getDomains():
        destinationDomains.append(domain["url"])
    e["destinationDomains"] = destinationDomains
    
    found = False
    for edge in edges:
        if edge["source"] == e["source"] and edge["matchedOutput"] == e["matchedOutput"] and edge["sourceDomains"] == e["sourceDomains"] and edge["destination"] == e["destination"] and edge["matchedInput"] == e["matchedInput"] and edge["destinationDomains"] == e["destinationDomains"]:
            hsr = edge["successEdges"]/edge["numberOfDiscoveredEdges"]
            found = True
            break
    if found:
        return hsr
    else:
        hsr = 1.0
        return hsr
    
"""
Store discovered plans in files.
@graphsPlans: Plans to store.
""" 
def storePlansFile(graphsPlans,jobId,feedt,tk):
    edgesFromFile = []
    with open("../../Data/services-dataset/edges_"+str(feedt)+"_"+str(tk)+".json") as dataFile:    
        edgesFromFile = json.load(dataFile)
    plansToStore = []
    with open("../../Data/services-dataset/graphPlans_"+str(feedt)+"_"+str(tk)+".json") as dataFile:    
        plansToStore = json.load(dataFile)
    plans = graphsPlans["plans"]
    for k, graphPlan in plans.items():
        toStore = {}
        toStore["jobId"] = jobId
        toStore["planId"] = k
        toStore["state"] = graphPlan.getState()
        toStore["mark"] = graphPlan.getMark()
        
        domains = []
        for domain in graphPlan.getDomains():
            domains.append(domain)
        toStore["domains"] = domains
        
        inputs = []
        for input in graphPlan.getInputs()[:]:
            if "state" in input.keys():
                del input["state"]
            inputs.append(input)
        toStore["inputs"] = inputs
        
        outputs = []
        for output in graphPlan.getOutputs()[:]:
            if "state" in output.keys():
                del output["state"]
            outputs.append(output)
        toStore["outputs"] = outputs
        
        vertices = {}
        for k, vertex in graphPlan.getVertexs().items():
            
            if vertex.getName() == "initial":
                v = {}
                v["name"] = vertex.getName()
                v["outputs"] = vertex.getOutputs()
                v["domains"] = vertex.getDomains()
                vertices[k]=v
            elif vertex.getName() == "final":
                v = {}
                v["name"] = vertex.getName()
                v["inputs"] = vertex.getInputs()
                v["domains"] = vertex.getDomains()
                vertices[k]=v
            else:
                v = vertex.getService()  
                #del v["_id"]
                vertices[k]=v
        toStore["vertices"] = vertices
        
        edges = []
        for edge in graphPlan.getEdges():
            e = {}
            l = edge.getLink()
            e["source"]=edge.getSource().getName()
            e["matchedOutput"] = l["matchedOutput"]["type"]
            sourceDomains = []
            for domain in edge.getSource().getDomains():
                sourceDomains.append(domain["url"])
            e["sourceDomains"] = sourceDomains
            
            e["destination"]=edge.getDestination().getName()
            e["matchedInput"] = l["matchedInput"]["type"]
            destinationDomains = []
            for domain in edge.getDestination().getDomains():
                destinationDomains.append(domain["url"])
            e["destinationDomains"] = destinationDomains
            e["degree"] = edge.getDegree()
            
            found = False
            for edgeFromFile in edgesFromFile:
                if edgeFromFile["source"] == e["source"] and edgeFromFile["matchedOutput"] == e["matchedOutput"] and edgeFromFile["sourceDomains"] == e["sourceDomains"] and edgeFromFile["destination"] == e["destination"] and edgeFromFile["matchedInput"] == e["matchedInput"] and edgeFromFile["destinationDomains"] == e["destinationDomains"]:
                    edgeFromFile["numberOfDiscoveredEdges"] = edgeFromFile["numberOfDiscoveredEdges"] + 1 
                    found = True
                    break
            if found == False:
                e["numberOfDiscoveredEdges"] = 1
                e["nonSuccessEdges"] = 0
                e["successEdges"] = 0
                edgesFromFile.append(e)
            
            ed = {}
            ed["source"] = edge.getSource().getName()
            ed["destination"] = edge.getDestination().getName()
            ed["degree"] = edge.getDegree()
            link = {}
            matchedOutput = l["matchedOutput"]
            if "state" in matchedOutput.keys():
                del matchedOutput["state"] 
            link["matchedOutput"] = matchedOutput
            matchedInput = l["matchedInput"]
            if "state" in matchedInput.keys():
                del matchedInput["state"] 
            link["matchedInput"]= matchedInput
            ed["link"] = link
            edges.append(ed)
        toStore["edges"] = edges
        plansToStore.append(toStore)
    with open("../../Data/services-dataset/graphPlans_"+str(feedt)+"_"+str(tk)+".json", "w") as outfile:
            json.dump(plansToStore, outfile, indent=3)
    with open("../../Data/services-dataset/edges_"+str(feedt)+"_"+str(tk)+".json", "w") as outfile:
            json.dump(edgesFromFile, outfile, indent=3)
        
"""
Update Feedback File.
@jobId: Job Id of the feedback.
@planId: Discovered plan that receives the feedback.
@feedback: Feedback value:
    0 - Unsuccess.
    1 - Success.
"""
def updateFeedbackFile(jobId,planId,feedback,feedt,tk):
    plans = []
    with open("../../Data/services-dataset/graphPlans_"+str(feedt)+"_"+str(tk)+".json") as dataFile:    
        plans = json.load(dataFile)
    edgesFromFile = []
    with open("../../Data/services-dataset/edges_"+str(feedt)+"_"+str(tk)+".json") as dataFile:    
        edgesFromFile = json.load(dataFile)
    for plan in plans:
        found = False
        if planId == plan["planId"] and jobId == plan["jobId"]:
            for edge in plan["edges"]:
                e = {}
                l = edge["link"]
                e["matchedOutput"] = l["matchedOutput"]["type"]
            
                vertices = plan["vertices"]
            
                source = vertices[edge["source"]]
                e["source"]=edge["source"]
                sourceDomains = []
                for domain in source["domains"]:
                    sourceDomains.append(domain["url"])
                e["sourceDomains"] = sourceDomains
            
                e["matchedInput"] = l["matchedInput"]["type"]
                destinationDomains = []
                destination = vertices[edge["destination"]]
                e["destination"]=edge["destination"]
                for domain in destination["domains"]:
                    destinationDomains.append(domain["url"])
                e["destinationDomains"] = destinationDomains
            
                if feedback == 0:
                    for edgeFromFile in edgesFromFile:
                        if edgeFromFile["source"] == e["source"] and edgeFromFile["matchedOutput"] == e["matchedOutput"] and edgeFromFile["sourceDomains"] == e["sourceDomains"] and edgeFromFile["destination"] == e["destination"] and edgeFromFile["matchedInput"] == e["matchedInput"] and edgeFromFile["destinationDomains"] == e["destinationDomains"]:
                            edgeFromFile["nonSuccessEdges"] = edgeFromFile["nonSuccessEdges"] + 1
                            found = True 
                            break
                
                if feedback == 1:
                    for edgeFromFile in edgesFromFile:
                        if edgeFromFile["source"] == e["source"] and edgeFromFile["matchedOutput"] == e["matchedOutput"] and edgeFromFile["sourceDomains"] == e["sourceDomains"] and edgeFromFile["destination"] == e["destination"] and edgeFromFile["matchedInput"] == e["matchedInput"] and edgeFromFile["destinationDomains"] == e["destinationDomains"]:
                            edgeFromFile["successEdges"] = edgeFromFile["successEdges"] + 1
                            found = True 
                            break
    with open("../../Data/services-dataset/edges_"+str(feedt)+"_"+str(tk)+".json", "w") as outfile:
            json.dump(edgesFromFile, outfile, indent=3)                    
        
        