"""
Checker class
This class implements the checking of discovered plans with the expected for a request.

Author: Christian Cabrera
"""

import json
import time
import os
import math

def getRelevantPlans(request):
    relevants = []
    services = request["services"]
    id = request["id"]
    """id = id.replace("_request","")"""
    if services == "1":
        with open("../../Data/requests-dataset/iot-responses/responses_" + services+".json") as dataFile:    
                    responsesFile = json.load(dataFile)
        response = responsesFile[id]
        offers = response["offers"]
        for offer in offers:
            if offer["relevant"] == 1:
                relevants.append(offer["graph"])
    else:
        with open("../../Data/requests-dataset/iot-responses/responses_" + services+".json") as dataFile:    
                    responsesFile = json.load(dataFile)
        file = request["file"]
        file = file.replace(".json","")
        response = responsesFile[file]
        offers = response["offers"]
        for offer in offers:
            if offer["relevant"] == 1:
                relevant = []
                graph = offer["graph"]
                for edge in graph:
                    relevant.append(edge)
                relevants.append(relevant)
    return relevants

def getRelevantServices(request):
    relevants = []
    services = request["services"]
    id = request["id"]
    id = id.replace("_request","")
    if services == "1":
        with open("../../Data/requests-dataset/iot-responses/responses_" + services+".json") as dataFile:    
            responsesFile = json.load(dataFile)
        response = responsesFile[id+"_request"]
        offers = response["offers"]
        for offer in offers:
            if offer["relevant"] == 1:
                graph = offer["graph"]
                for edge in graph:
                    services = edge.split(" to ")
                    for service in services:
                        service = service.replace("from", "")
                        if "initial" not in service and "final" not in service:
                            found = False
                            for rel in relevants:
                                if rel.replace(" ","") == service.replace(" ",""):
                                    found = True
                                    break
                            if not found:
                                relevants.append(service) 
    else:
        with open("../../Data/requests-dataset/iot-responses/responses_" + services+".json") as dataFile:    
                    responsesFile = json.load(dataFile)
        file = request["file"]
        file = file.replace(".json","")
        response = responsesFile[file]
        offers = response["offers"]
        for offer in offers:
            if offer["relevant"] == 1:
                graph = offer["graph"]
                for edge in graph:
                    services = edge.split(" to ")
                    for service in services:
                        service = service.replace("from", "")
                        if "initial" not in service and "final" not in service:
                            found = False
                            for rel in relevants:
                                if rel.replace(" ","") == service.replace(" ",""):
                                    found = True
                                    break
                            if not found:
                                relevants.append(service)
    return relevants

def checkPlan(plan,relevants,services):
    if services == "1":
        links=[]
        for edge in plan["edges"]:
            link = ""
            link = "from " + edge["source"] + " to " + edge["destination"]
            links.append(link)
        for relevant in relevants:
            if len(links) == len(relevant):
                matched = 0
                for link in links:
                    if link in relevant:
                        matched = matched + 1
                if matched == len(links):
                    return True
        """for k,vertex in plan["vertices"].items():
            if k != "initial" and k!="final":
                file = vertex["file"]
                print("File: "+file)
                for relevant in relevants:
                    if file in relevant:
                        return True"""
    else:
        links=[]
        for edge in plan["edges"]:
            link = ""
            link = "from " + edge["source"] + " to " + edge["destination"]
            links.append(link)                            
        for relevant in relevants:
            if len(links) == len(relevant):
                matched = 0
                for link in links:
                    if link in relevant:
                        matched = matched + 1
                if matched == len(links):
                    return True
    return False
