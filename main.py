"""
Service Discovery Request
This project implements a client who request a service.

Author: Christian Cabrera
"""

import sys
import json
import paho.mqtt.client as mqttClient
import os
import time
import csv
import random
import xlsxwriter
import logic.Register as register
import logic.Checker as checker
import logic.Planner as planner
import logic.FeedbackManager as feedbackManager

from random import choice
from random import randint
from pprint import pprint
from plainbox.impl import job

subscriberConnected = False #global variable for the state of the connection
publisherConnected = False #global variable for the state of the connection
clientId = "client_01_"
sendRequest = True
newPar = 0
numberOfRequests = 0
numberOfGateways=4
numberOfResponses = 0
limitOfRequests = 10
results = []
similarityThreshold = 0.8
""" 
    Specifies where the experiments run
    Possible values tchpc, desktop or pi
"""
platform = "pi"

""" 
    Specifies the approach to evaluate
    Possible values classic (0 to 4) or heuristic (5 to 9)
        0 - Classic Semantic matchmaker (Logic).
        1 - Classic Cosine Similarity (Non-logic).
        2 - Classic Jaccard Index (Non-Logic).
        3 - Classic Semantic matchmaker and Cosine Similarity (Hybrid).
        4 - Classic Semantic matchmaker and Jaccard Index (Hybrid).
        5 - Heuristic Semantic matchmaker (Logic).
        6 - Heuristic Cosine Similarity (Non-logic).
        7 - Heuristic Jaccard Index (Non-Logic).
        8 - Heuristic Semantic matchmaker and Cosine Similarity (Hybrid).
        9 - Heuristic Semantic matchmaker and Jaccard Index (Hybrid).
        10 - Coversation Semantic matchmaker (Logic).
        11 - Coversation Cosine Similarity (Non-logic).
        12 - Coversation Jaccard Index (Non-Logic).
        13 - Coversation Semantic matchmaker and Cosine Similarity (Hybrid).
        14 - Coversation Semantic matchmaker and Jaccard Index (Hybrid).
        15 - ACO Semantic matchmaker (Logic).
        16 - ACO Cosine Similarity (Non-logic).
        17 - ACO Jaccard Index (Non-Logic).
        18 - ACO Semantic matchmaker and Cosine Similarity (Hybrid).
        19 - ACO Semantic matchmaker and Jaccard Index (Hybrid).
"""
discoveryType = 3
approach = "classic"

"""
    IP addresses to allow the MQTT communication when experiments run on desktop or pi
    When desktop there is one sde and consumer which is localhost
    When pi the first sde and the consumer are in the same pi 192.169.3.6
"""
#sde_address1= "localhost"
sde_address1= "192.168.3.2"
sde_address2= "192.168.3.3"
sde_address3= "192.168.3.4"
sde_address4= "192.168.3.5"
#sde_address5= "192.168.3.10"
#consumerAddress= "localhost"
consumerAddress= "192.168.3.1"
port = 1883
user = ""
password = ""

def utf8len(s):
    return len(s.encode('UTF-8'))

def on_publisher_connect(client, userdata, flags, rc):
    if rc == 0:
        #print("Connected to broker, ready to publish messages...")
        global publisherConnected                
        publisherConnected = True                
    else:
        print("Connection failed")

def publishMessage(broker_address,port,topic,message):
    publisher = mqttClient.Client()               
    publisher.on_connect= on_publisher_connect               
    publisher.connect(broker_address, port=port)
    publisher.loop_start()
    while publisherConnected != True:    
        time.sleep(0.1)
    publisher.publish(topic, payload=message)
    time.sleep(0.1)
    publisher.disconnect()
    publisher.loop_stop()
    global publisherConnected                
    publisherConnected = False
    
def on_subscriber_connect(client, userdata, flags, rc):
    if rc == 0:
         global subscriberConnected                
         subscriberConnected = True               
    else:
        print("Connection failed")
         
def on_message_received(client, userdata, message):
    client.disconnect()
    client.loop_stop()
    global subscriberConnected                
    subscriberConnected = False
    receivedTime = int(round(time.time() * 1000))
    message = str(message.payload).replace("b'", "").replace("'","").replace("\\","")
    jsonMessage=json.loads(message)
    messageType = jsonMessage["messageType"]
    if messageType == "response":
        plans= []
        plans = jsonMessage["jsonPlans"]
        if len(plans)>0:
            solved = jsonMessage["solved"]
            gatewayAddress = jsonMessage["gatewayAddress"]
            jobId = jsonMessage["jobId"]
            if solved:
                jsonRequest = jsonMessage["jsonRequest"]
                discoveryType = jsonRequest["discoveryType"]
                """ Get original request size."""
                requestSize = jsonRequest['requestSize']
                """ Calculate response time."""
                requestTime = jsonRequest['requestTime']
                discoveryTime = jsonRequest['discoveryTime']
                responseTime = receivedTime - requestTime
                print("Response message is here for JobId: " + jobId + "/" + str(responseTime))
                global numberOfResponses
                numberOfResponses = numberOfResponses + 1;
                """ Check response accuracy."""
                precision = 0.0
                recall = 0.0
                truePositives = 0.0
                retrieved = len(plans)
                if discoveryType <= 9 or discoveryType >=15:
                    relevants = checker.getRelevantPlans(jsonRequest)
                    feedbackPlans = []
                    for plan in plans:
                        fb = {}
                        fb["planId"] = plan["planId"]
                        if checker.checkPlan(plan,relevants,jsonRequest["services"]):
                            truePositives = truePositives + 1
                            fb["feedback"] = 1
                        else:
                            fb["feedback"] = 0
                        feedbackPlans.append(fb)
                    precision = truePositives/retrieved
                    recall = truePositives/len(relevants)
                    """ Sending feedback."""
                    if discoveryType >= 5 and discoveryType <= 9:
                        feedback = {}
                        feedback["jobId"] = jobId
                        feedback["plans"] = feedbackPlans
                        feedback["consumerAddress"] = consumerAddress
                        feedback["messageType"] = "feedback"
                        requestSize = requestSize + utf8len(str(feedback))
                        message = json.dumps(feedback)
                        print("Sending Feedback:  " + jobId)
                        print("Waiting Feedback ACK:  " + jobId)
                        subscribeToMessage(consumerAddress,port,"/discovery/feedbackACK/+")
                        publishMessage(consumerAddress,port,"/discovery/feedback/"+jobId,message)
                    else:
                        time.sleep(1.5)
                        global sendRequest
                        sendRequest = True
                    del feedbackPlans
                else:
                    solvedReq = jsonMessage["solvedReq"]
                    solvedTasks = solvedReq["tasks"]
                    retrievedServices = []
                    sol = True
                    for task in solvedTasks:
                        servicesTasks = task["services"]
                        if len(servicesTasks) == 0:
                            sol = False
                            break
                        for service in servicesTasks:
                            retrievedServices.append(service["name"])
                        task["services"] = []
                                
                    if sol:
                        relevantsServices = checker.getRelevantServices(jsonRequest)
                        for retrievedService in retrievedServices:
                            for relevantService in relevantsServices:
                                if relevantService.replace(" ","") == retrievedService.replace(" ",""):
                                    truePositives = truePositives + 1
                                    break 
                        """ Calculate and recall."""
                        retrieved = len(retrievedServices)
                        precision = truePositives/retrieved
                        recall = truePositives/len(relevantsServices)
                    time.sleep(1.5)
                    global sendRequest
                    sendRequest = True        
                """ Save response time and accuracy."""
                global results
                result = [jobId,str(precision),str(recall),str(responseTime),str(responseTime),str(requestSize),str(jsonRequest["services"]),jsonRequest["file"]]
                results.append(result)
                print(" Total responses: " + str(numberOfResponses))
            else:
                jobId = jsonMessage["jobId"]
                print("Incomplete response message is here for JobId: " + jobId)
                jsonRequest = jsonMessage["jsonRequest"]
                discoveryType = jsonRequest["discoveryType"]
                """ Get original request size."""
                requestSize = jsonRequest['requestSize']
                """ Calculate response time."""
                requestTime = jsonRequest['requestTime']
                discoveryTime = jsonRequest['discoveryTime']
                responseTime = receivedTime - requestTime
                print("Partial response message is here for JobId: " + jobId + "/" + str(responseTime))
                global numberOfResponses
                numberOfResponses = numberOfResponses + 1;
                """ Precision and recall to 0."""
                precision = 0.0
                recall = 0.0
                """ Sending feedback."""
                if discoveryType >= 5 and discoveryType <= 9:
                    feedbackPlans = []
                    for plan in plans:
                        fb={}
                        fb["planId"] = plan["planId"]
                        fb["feedback"] = 0
                        feedbackPlans.append(fb)
                    feedback = {}
                    feedback["jobId"] = jobId
                    feedback["plans"] = feedbackPlans
                    feedback["consumerAddress"] = consumerAddress
                    feedback["messageType"] = "feedback"
                    requestSize = requestSize + utf8len(str(feedback))
                    message = json.dumps(feedback)
                    print("Sending Feedback:  " + jobId)
                    print("Waiting Feedback ACK:  " + jobId)
                    subscribeToMessage(consumerAddress,port,"/discovery/feedbackACK/+")
                    publishMessage(consumerAddress,port,"/discovery/feedback/"+jobId,message)
                    del feedbackPlans
                else:
                    time.sleep(1.5)
                    global sendRequest
                    sendRequest = True
                """ Save response time and accuracy."""
                global results
                result = [jobId,str(precision),str(recall),str(responseTime),str(responseTime),str(requestSize),str(jsonRequest["services"]),jsonRequest["file"]]
                results.append(result)
                print(" Total responses: " + str(numberOfResponses))
        else:
            global numberOfRequests
            numberOfRequests = numberOfRequests - 1;
            jobId = jsonMessage["jobId"]
            jsonRequest = jsonMessage["jsonRequest"]
            discoveryType = jsonRequest["discoveryType"]
            print("Empty response message is here for JobId: " + jobId)
            if discoveryType >= 5:
                feedbackPlans = []
                for plan in plans:
                    fb = {}
                    fb["planId"] = plan["planId"]
                    fb["feedback"] = 0
                    feedbackPlans.append(fb)
                feedback = {}
                feedback["jobId"] = jobId
                feedback["plans"] = feedbackPlans
                feedback["consumerAddress"] = consumerAddress
                feedback["messageType"] = "feedback"
                message = json.dumps(feedback)
                print("Sending Feedback:  " + jobId)
                print("Waiting Feedback ACK:  " + jobId)
                subscribeToMessage(consumerAddress,port,"/discovery/feedbackACK/+")
                publishMessage(consumerAddress,port,"/discovery/feedback/"+jobId,message)
                del feedbackPlans
            else:
                time.sleep(1.5)
                global sendRequest
                sendRequest = True
        del plans
    if messageType == "feedbackACK":
        time.sleep(0.5)
        global sendRequest
        sendRequest = True
    if messageType == "removeACK":
        time.sleep(60)
        #time.sleep(30)
        global sendRequest
        sendRequest = True
    if messageType == "removeServicesACK":
        time.sleep(60)
        #time.sleep(30)
        global sendRequest
        sendRequest = True

def subscribeToMessage(broker_address,port,topic):
    subscriber = mqttClient.Client()               
    subscriber.on_connect= on_subscriber_connect                      
    subscriber.on_message= on_message_received
    subscriber.connect(broker_address, port=port)
    subscriber.loop_start()
    while subscriberConnected != True:    
        time.sleep(0.1)
    subscriber.subscribe(topic)

def writeResults(fileName,remove):
    try:
        workbook = xlsxwriter.Workbook("./results/" + fileName + ".xlsx")
        worksheet = workbook.add_worksheet("Results")
        row = 0;
        worksheet.write(row, 0, "Request")
        worksheet.write(row, 1, "Precision")
        worksheet.write(row, 2, "Recall")
        worksheet.write(row, 3, "Response Time")
        worksheet.write(row, 4, "Discovery Time")
        worksheet.write(row, 5, "Communication Time")
        worksheet.write(row, 6, "Request Size")
        worksheet.write(row, 7, "Number of Services")
        worksheet.write(row, 8, "Request File")
        row = row + 1
        for item in results:
            worksheet.write_string(row, 0, item[0].replace("'",""))
            worksheet.write_number(row, 1, float(item[1]))
            worksheet.write_number(row, 2, float(item[2]))
            worksheet.write_number(row, 3, float(item[3]))
            worksheet.write_number(row, 4, float(item[4]))
            worksheet.write_formula(row, 5, "=D"+str(row+1)+" - E"+str(row+1))
            worksheet.write_number(row, 6, float(item[5]))
            worksheet.write_string(row, 7, item[6])
            worksheet.write_string(row, 8, item[7])
            row = row + 1
        worksheet.write(row, 0, "AVERAGE")
        worksheet.write_formula(row, 1, 'AVERAGE(B2:B'+str(row)+')')
        worksheet.write_formula(row, 2, 'AVERAGE(C2:C'+str(row)+')')
        worksheet.write_formula(row, 3, 'AVERAGE(D2:D'+str(row)+')')
        worksheet.write_formula(row, 4, 'AVERAGE(E2:E'+str(row)+')')
        worksheet.write_formula(row, 5, 'AVERAGE(F2:F'+str(row)+')')
        worksheet.write_formula(row, 6, 'AVERAGE(G2:G'+str(row)+')')
        row = row + 1
        
        worksheet.write(row, 0, "MIN")
        worksheet.write_formula(row, 1, "QUARTILE(B2:B"+str(row-1)+",0)")
        worksheet.write_formula(row, 2, "QUARTILE(C2:C"+str(row-1)+",0)")
        worksheet.write_formula(row, 3, "QUARTILE(D2:D"+str(row-1)+",0)")
        worksheet.write_formula(row, 4, "QUARTILE(E2:E"+str(row-1)+",0)")
        worksheet.write_formula(row, 5, "QUARTILE(F2:F"+str(row-1)+",0)")
        worksheet.write_formula(row, 6, "QUARTILE(G2:G"+str(row-1)+",0)")
        row = row + 1
        worksheet.write(row, 0, "Q1")
        worksheet.write_formula(row, 1, "QUARTILE(B2:B"+str(row-2)+",1)")
        worksheet.write_formula(row, 2, "QUARTILE(C2:C"+str(row-2)+",1)")
        worksheet.write_formula(row, 3, "QUARTILE(D2:D"+str(row-2)+",1)")
        worksheet.write_formula(row, 4, "QUARTILE(E2:E"+str(row-2)+",1)")
        worksheet.write_formula(row, 5, "QUARTILE(F2:F"+str(row-2)+",1)")
        worksheet.write_formula(row, 6, "QUARTILE(G2:G"+str(row-2)+",1)")
        row = row + 1
        worksheet.write(row, 0, "MEDIAN")
        worksheet.write_formula(row, 1, "QUARTILE(B2:B"+str(row-3)+",2)")
        worksheet.write_formula(row, 2, "QUARTILE(C2:C"+str(row-3)+",2)")
        worksheet.write_formula(row, 3, "QUARTILE(D2:D"+str(row-3)+",2)")
        worksheet.write_formula(row, 4, "QUARTILE(E2:E"+str(row-3)+",2)")
        worksheet.write_formula(row, 5, "QUARTILE(F2:F"+str(row-3)+",2)")
        worksheet.write_formula(row, 6, "QUARTILE(G2:G"+str(row-3)+",2)")
        row = row + 1
        worksheet.write(row, 0, "Q3")
        worksheet.write_formula(row, 1, "QUARTILE(B2:B"+str(row-4)+",3)")
        worksheet.write_formula(row, 2, "QUARTILE(C2:C"+str(row-4)+",3)")
        worksheet.write_formula(row, 3, "QUARTILE(D2:D"+str(row-4)+",3)")
        worksheet.write_formula(row, 4, "QUARTILE(E2:E"+str(row-4)+",3)")
        worksheet.write_formula(row, 5, "QUARTILE(F2:F"+str(row-4)+",3)")
        worksheet.write_formula(row, 6, "QUARTILE(G2:G"+str(row-4)+",3)")
        row = row + 1
        worksheet.write(row, 0, "MAX")
        worksheet.write_formula(row, 1, "QUARTILE(B2:B"+str(row-5)+",4)")
        worksheet.write_formula(row, 2, "QUARTILE(C2:C"+str(row-5)+",4)")
        worksheet.write_formula(row, 3, "QUARTILE(D2:D"+str(row-5)+",4)")
        worksheet.write_formula(row, 4, "QUARTILE(E2:E"+str(row-5)+",4)")
        worksheet.write_formula(row, 5, "QUARTILE(F2:F"+str(row-5)+",4)")
        worksheet.write_formula(row, 6, "QUARTILE(G2:G"+str(row-5)+",4)")
        workbook.close()
        if remove:
            global results
            results = []
    except IOError:
            print("I/O error")    
    return

def main(feedt, topk, funt, nser,length):
    """ If approach is tchpc:
            Experiments run in a high performance computing node where there is no mongo or mosquitto 
            So, it uses the planner in this project avoiding communication through MQTT and files instead of database avoiding MongoDB
            It performs the process in a centralised fashion
        If approach is desktop:
            Experiments run in a centralised pc where mongo and mosquitto are available
            So, it sends discovery messages and store the information for the process in a centralised fashion
        If approach is pi:
            Experiments run in a network of raspberries where mongo and mosquitto are available
            So, it sends discovery messages between gateways and store the information for the process in a distributed fashion
    """
    if platform == "tchpc":
        if discoveryType <= 4:
            requests = {}
            """folders = os.listdir("../../Data/requests-dataset/json-requests-tchpc/")
            folders = os.listdir("../../Data/requests-dataset/json-requests-tchpc/")
            req = 1
            for folder in folders:
                files = os.listdir("../../Data/requests-dataset/json-requests-tchpc/"+folder+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/json-requests-tchpc/" + folder + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
                    
            req = 1
            l = 1
            while l <= 5:
                requests = {}
                files = os.listdir("../../Data/requests-dataset/all-requests/"+str(l)+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1
        
                servicesPerGateway = 10000
                while servicesPerGateway <= 10000:
                    r = 1
                    while r <= 1:
                        currentReq = 1
                        global numberOfResponses
                        numberOfResponses = 0
                        global numberOfRequests
                        numberOfRequests = 0
                        while(numberOfResponses < limitOfRequests):
                            request = {}
                            """if currentReq == req:
                                currentReq = 1"""
                            request = requests[randint(1, (req - 1))]
                            print("\n*** Approach " + approach + "::Services " + str(servicesPerGateway) +" Length " +str(l)+ "::Round " + str(r) + "::Number of Requests: " + str(numberOfRequests + 1) + " ***")
                            # currentReq = currentReq + 1
                            jobId = clientId + str(numberOfRequests)
                            graphsPlans = {}
                            startTime = int(round(time.time() * 1000))
                            print("\n Waiting for planner::" + request["services"] + "::" + request["file"])
                            graphsPlans["plans"] = planner.backwardPlanningClassic2(request, {}, discoveryType, similarityThreshold, int(request["services"]), servicesPerGateway)
                            endTime = int(round(time.time() * 1000))
                            requestTime = endTime - startTime
                            requestSize = utf8len(str(request))
                            plans = []
                            plans = planner.graphsToJsonPlans(graphsPlans, jobId)
                            if(len(plans) > 0):
                                global numberOfResponses
                                numberOfResponses = numberOfResponses + 1; 
                                global numberOfRequests
                                numberOfRequests = numberOfRequests + 1;
                                truePositives = 0.0
                                feedbackPlans = []
                                relevants = checker.getRelevantPlans(request)
                                for plan in plans:
                                    if plan["state"] == 1:
                                        """ Check response accuracy."""
                                        if checker.checkPlan(plan, relevants, request["services"]):
                                            truePositives = truePositives + 1
                                            plan["feedback"] = 1
                                        else:
                                            plan["feedback"] = 0
                                            feedbackPlans.append(plan)
                                    else:
                                        plan["feedback"] = 0
                                        feedbackPlans.append(plan)
                                """ Calculate and recall."""
                                retrieved = len(plans)
                                precision = truePositives / retrieved
                                recall = truePositives / len(relevants)        
                                """ Save response time and accuracy."""
                                global results
                                result = [jobId, str(precision), str(recall), str(requestTime), str(requestTime), str(requestSize), str(request["services"]), request["file"]]
                                results.append(result)
                                """ Write results' file at the end."""
                                fileName = "results_" + platform + "_services_" + str(servicesPerGateway) + "_" + approach + "_length_" + str(l) + "_round_" + str(r)
                                writeResults(fileName, False)
                        """ Write results' file at the end."""
                        fileName = "results_" + platform + "_services_" + str(servicesPerGateway) + "_" + approach + "_length_" + str(l) + "_round_" + str(r)
                        writeResults(fileName, True)
                        r = r + 1
                    servicesPerGateway = servicesPerGateway + 2000
                l = l + 1
            print("finished")
        elif discoveryType <= 9:
            print("here")
            matchingType = 0
            if(discoveryType == 5):
                matchingType = 0
            if(discoveryType == 6):
                matchingType = 1
            if(discoveryType == 7):
                matchingType = 2
            if(discoveryType == 8):
                matchingType = 3
            if(discoveryType == 9):
                matchingType = 4
            requests = {}
            """folders = os.listdir("../../Data/requests-dataset/json-requests/")
            req = 1
            for folder in folders:
                files = os.listdir("../../Data/requests-dataset/json-requests/"+folder+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/json-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
            req = 1
            l = 1
            while l <= 5:
                requests = {}
                files = os.listdir("../../Data/requests-dataset/all-requests/"+str(l)+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1
                functionalThreshold = funt
                while functionalThreshold>=0:
                    feedbackThreshold = feedt
                    while feedbackThreshold >= feedt:
                        topK = tk
                        while topK >= tk:
                            servicesPerGateway = nser
                            while servicesPerGateway <= 10000:
                                register.cleanHistoricalFiles(feedt,tk)
                                r = 1
                                while r <= 5:
                                    currentReq = 1
                                    global numberOfResponses
                                    numberOfResponses = 0
                                    global numberOfRequests
                                    numberOfRequests = 0
                                    global sendRequest
                                    sendRequest = True
                                    while(numberOfResponses < limitOfRequests):
                                        request = {}
                                        """if currentReq == req:
                                            currentReq = 1"""
                                        request = requests[randint(1,(req-1))]
                                        #currentReq = currentReq + 1
                                        print("\n*** Approach "+approach+"::Services "+str(servicesPerGateway) +" Length " +str(l)+" TopK "+str(topK)+"::FeedbackThreshold "+str(feedbackThreshold/10)+"::FunctionalThreshold "+str(functionalThreshold/10)+"::Round " + str(r) +"::Number of Requests: " + str(numberOfRequests+1) + " ***")
                                        jobId = clientId + str(numberOfRequests)
                                        graphsPlans = {}
                                        startTime = int(round(time.time() * 1000))
                                        print("\n Waiting for planner::"+request["services"]+"::"+request["file"])
                                        graphsPlans["plans"] = planner.backwardPlanningHeuristic(jobId,request,{},matchingType,similarityThreshold,(feedbackThreshold/10), topK, (functionalThreshold/10),servicesPerGateway,feedt,tk)
                                        endTime = int(round(time.time() * 1000))
                                        requestTime = endTime - startTime
                                        requestSize = utf8len(str(request))
                                        feedbackManager.storePlansFile(graphsPlans,jobId,feedt,tk)
                                        plans = [] 
                                        plans = planner.graphsToJsonPlans(graphsPlans,jobId)
                                        if(len(plans)>0):
                                            global numberOfResponses
                                            numberOfResponses = numberOfResponses + 1;
                                            global numberOfRequests
                                            numberOfRequests = numberOfRequests + 1;
                                            truePositives = 0.0
                                            feedbackPlans = []
                                            relevants = checker.getRelevantPlans(request)
                                            for plan in plans:
                                                if plan["state"] == 1:
                                                    """ Check response accuracy."""
                                                    if checker.checkPlan(plan,relevants,request["services"]):
                                                        truePositives = truePositives + 1
                                                        plan["feedback"] = 1
                                                    else:
                                                        plan["feedback"] = 0
                                                else:
                                                    plan["feedback"] = 0
                                                feedbackPlans.append(plan)
                                            """ Calculate and recall."""
                                            retrieved = len(plans)
                                            precision = truePositives/retrieved
                                            recall = truePositives/len(relevants)        
                                            """ Storing feedback."""
                                            for plan in feedbackPlans:
                                                requestSize = requestSize + utf8len(str(jobId)) + utf8len(str(plan["planId"])) + utf8len(str(plan["feedback"]))
                                                feedbackManager.updateFeedbackFile(jobId,plan["planId"],plan["feedback"],feedt,tk)
                                            del feedbackPlans
                                            """ Save response time and accuracy."""
                                            global results
                                            result = [jobId,str(precision),str(recall),str(requestTime),str(requestTime),str(requestSize),str(request["services"]),request["file"]]
                                            results.append(result)
                                    """ Write results' file at the end."""
                                    fileName = "results_"+platform+"_services_"+str(servicesPerGateway)+ "_length_" + str(l) + "_topK_" + str(topK) +"_"+approach + "_feedt_" + str(feedbackThreshold/10)+"_"+ "_funct_" + str(functionalThreshold/10)+"_round_" + str(r)
                                    writeResults(fileName,True)
                                    r = r + 1
                                servicesPerGateway = servicesPerGateway + 2000
                            topK = topK - 10
                        feedbackThreshold = feedbackThreshold - 2
                    functionalThreshold = functionalThreshold - 2
                l =l + 1
            print("finished")
        else:
            """requests = {}
            folders = os.listdir("../../Data/requests-dataset/conversations-request/")
            req = 1
            for folder in folders:
                files = os.listdir("../../Data/requests-dataset/conversations-request/"+folder+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/conversations-request/" + folder + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
            matchingType = 0
            if(discoveryType == 10):
                matchingType = 0
            if(discoveryType == 11):
                matchingType = 1
            if(discoveryType == 12):
                matchingType = 2
            if(discoveryType == 13):
                matchingType = 3
            if(discoveryType == 14):
                matchingType = 4
            req = 1
            l = 1
            while l <= 5:
                requests = {}
                files = os.listdir("../../Data/requests-dataset/all-requests/"+str(l)+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1
                r = 1
                while r <= 5:
                    servicesPerGateway = 2000
                    while servicesPerGateway <= 10000:
                        currentReq = 1
                        global numberOfResponses
                        numberOfResponses = 0
                        global numberOfRequests
                        numberOfRequests = 0
                        while(numberOfResponses < limitOfRequests):
                            request = {}
                            """if currentReq == req:
                                currentReq = 1"""
                            request = requests[randint(1,(req-1))]
                        
                            print("\n*** Approach " + approach + "::Services " + str(servicesPerGateway) +" Length " +str(l)+"::Round " + str(r) +"::Number of Requests: " + str(numberOfRequests+1) + " ***")
                            #currentReq = currentReq + 1
                            jobId = clientId + str(numberOfRequests)
                            graphsPlans = {}
                            startTime = int(round(time.time() * 1000))
                            print("\n Waiting for conversation::"+request["services"]+"::"+request["file"])
                            solvedReq = planner.conversationApproach(request,matchingType,similarityThreshold,servicesPerGateway)
                            endTime = int(round(time.time() * 1000))
                            requestTime = endTime - startTime
                            requestSize = utf8len(str(request))
                            solvedTasks = solvedReq["tasks"]
                            retrievedServices = []
                            sol = True
                            for task in solvedTasks:
                                servicesTasks = task["services"]
                                if len(servicesTasks) == 0:
                                    sol = False
                                    break
                                for service in servicesTasks:
                                    retrievedServices.append(service["name"])
                                task["services"] = []
                                
                            if sol:
                                global numberOfResponses
                                numberOfResponses = numberOfResponses + 1; 
                                global numberOfRequests
                                numberOfRequests = numberOfRequests + 1;
                                truePositives = 0.0
                                relevantsServices = checker.getRelevantServices(request)
                                for retrievedService in retrievedServices:
                                    for relevantService in relevantsServices:
                                        if relevantService.replace(" ","") == retrievedService.replace(" ",""):
                                            truePositives = truePositives + 1
                                            break 
                                """ Calculate and recall."""
                                retrieved = len(retrievedServices)
                                precision = truePositives/retrieved
                                recall = truePositives/len(relevantsServices)        
                                """ Save response time and accuracy."""
                                global results
                                result = [jobId,str(precision),str(recall),str(requestTime),str(requestTime),str(requestSize),str(request["services"]),request["file"]]
                                results.append(result)
                        """ Write results' file at the end."""
                        fileName = "results_"+platform+"_services_"+str(servicesPerGateway) + "_length_" + str(l) + "_"+approach + "_round_" + str(r)
                        writeResults(fileName,True)
                        servicesPerGateway = servicesPerGateway + 2000
                    r = r + 1
                l = l + 1
            print("finished")
    elif platform == "desktop":
        if discoveryType <= 4:
            """requests = {}
            folders = os.listdir("../../Data/requests-dataset/json-requests-tchpc/")
            req = 1
            for folder in folders:
                files = os.listdir("../../Data/requests-dataset/json-requests-tchpc/"+folder+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/json-requests-tchpc/" + folder + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
            req = 1
            l = 1
            while l <= 5:
                requests = {}
                files = os.listdir("../../Data/requests-dataset/all-requests/"+str(l)+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1
                servicesPerGateway = 2000
                while servicesPerGateway <= 10000:
                    register.removeAllServices()
                    register.serviceRegistration(servicesPerGateway)
                    r = 1
                    while r <= 1:
                        currentReq = 1
                        global numberOfResponses
                        numberOfResponses = 0
                        global numberOfRequests
                        numberOfRequests = 0
                        global sendRequest
                        sendRequest = True
                        while(numberOfResponses < limitOfRequests):
                            if sendRequest == True:
                                print("\n*** Approach " + approach + "::Services " + str(servicesPerGateway) + " Length " + str(l) + "::Round " + str(r) + "::Number of Requests: " + str(numberOfRequests + 1) + " ***")
                                try:
                                    request = {}
                                    """if currentReq == req:
                                        currentReq = 1"""
                                    time.sleep(3)
                                    request = requests[randint(1, (req - 1))]
                                    # currentReq = currentReq + 1
                                    global numberOfRequests
                                    numberOfRequests = numberOfRequests + 1
                                    jobId = clientId + str(numberOfRequests)
                                    request["jobId"] = jobId
                                    request["consumerAddress"] = consumerAddress
                                    request["messageType"] = "request"
                                    requestTime = int(round(time.time() * 1000))
                                    request["requestTime"] = requestTime
                                    request["requestSize"] = utf8len(str(request))
                                    request["discoveryTime"] = 0
                                    request["discoveryType"] = discoveryType
                                    request["similarityThreshold"] = similarityThreshold
                                    message = json.dumps(request)
                                    subscribeToMessage(consumerAddress, port, "/discovery/response/+")
                                    publishMessage(sde_address1, port, "/discovery/request", message)                
                                    global sendRequest
                                    sendRequest = False
                                    print("Approach " + approach + "::Round " + str(r) + "::Published Request " + str(numberOfRequests) + ":" + str(currentReq - 1) + "::Request ID " + str(request["id"]))
                                    del request
                                except KeyboardInterrupt:
                                    print ("Exception sending the request")
                                    global sendRequest
                                    sendRequest = True 
                        """ Write results' file at the end."""
                        fileName = "results_" + platform + "_services_" + str(servicesPerGateway) + "_" + approach +"_length_" + str(l) +  "_round_" + str(r)
                        writeResults(fileName,True)
                        r = r + 1
                        while(sendRequest == False):
                            time.sleep(0.1)
                    servicesPerGateway = servicesPerGateway + 2000
                l = l + 1 
            register.removeAllServices()
            print("finished")
        elif discoveryType <= 9:
            """requests = {}
            folders = os.listdir("../../Data/requests-dataset/json-requests/")
            req = 1
            for folder in folders:
                files = os.listdir("../../Data/requests-dataset/json-requests/" + folder + "/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/json-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
            req = 1
            l = 1
            while l <= 5:
                requests = {}
                files = os.listdir("../../Data/requests-dataset/all-requests/"+str(l)+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1
                functionalThreshold = 10
                while functionalThreshold >= 0:
                    feedbackThreshold = 10
                    while feedbackThreshold >= 0:
                        topK = 25
                        while topK >= 5:
                            servicesPerGateway = 2000
                            while servicesPerGateway <= 10000:
                                register.removeAllServices()
                                register.serviceRegistration(servicesPerGateway)
                                r = 1
                                while r <= 10:
                                    currentReq = 1
                                    global numberOfResponses
                                    numberOfResponses = 0
                                    global numberOfRequests
                                    numberOfRequests = 0
                                    global sendRequest
                                    sendRequest = True
                                    while(numberOfResponses < limitOfRequests):
                                        if sendRequest == True:
                                            print("\n*** Approach "+approach+"::Services "+str(servicesPerGateway) +" Length " + str(l) + " TopK " + str(topK) + "::FeedbackThreshold " + str(feedbackThreshold / 10) + "::FunctionalThreshold " + str(functionalThreshold / 10) + "::Round " + str(r) + "::Number of Requests: " + str(numberOfRequests + 1) + " ***")
                                            try:
                                                request = {}
                                                """if currentReq == req:
                                                    currentReq = 1"""
                                                time.sleep(3)
                                                request = requests[randint(1, (req - 1))]
                                                # currentReq = currentReq + 1
                                                global numberOfRequests
                                                numberOfRequests = numberOfRequests + 1
                                                jobId = clientId + str(numberOfRequests)
                                                request["jobId"] = jobId
                                                request["consumerAddress"] = consumerAddress
                                                request["messageType"] = "request"
                                                requestTime = int(round(time.time() * 1000))
                                                request["requestTime"] = requestTime
                                                request["requestSize"] = utf8len(str(request))
                                                request["discoveryTime"] = 0
                                                request["discoveryType"] = discoveryType
                                                request["similarityThreshold"] = similarityThreshold
                                                request["feedbackThreshold"] = (feedbackThreshold / 10)
                                                request["functionalThreshold"] = (functionalThreshold / 10)
                                                request["topK"] = topK
                                                message = json.dumps(request)
                                                subscribeToMessage(consumerAddress, port, "/discovery/response/+")
                                                publishMessage(sde_address1, port, "/discovery/request", message)                
                                                global sendRequest
                                                sendRequest = False
                                                print("Published Request " + str(numberOfRequests) + ": " + str(currentReq - 1) + "/" + str(request["id"]))
                                                del request
                                            except KeyboardInterrupt:
                                                print ("Exception sending the request")
                                                global sendRequest
                                                sendRequest = True 
                                    """ Write results' file at the end."""
                                    fileName = "results_"+platform+"_services_"+str(servicesPerGateway)+ "_length_" + str(l) + "_topK_" + str(topK) + "_" + approach + "_feedt_" + str(feedbackThreshold / 10) + "_" + "_funct_" + str(functionalThreshold / 10) + "_round_" + str(r)
                                    writeResults(fileName,True)
                                    r = r + 1
                                    while(sendRequest == False):
                                        time.sleep(0.1)
                                servicesPerGateway = servicesPerGateway + 2000
                            topK = topK - 5
                        feedbackThreshold = feedbackThreshold - 2
                        del request
                        global sendRequest
                        sendRequest = False
                    functionalThreshold = functionalThreshold - 2
                l = l + 1
            register.removeAllServices()
            print("finished")
        elif discoveryType <= 14:
            """requests = {}
            folders = os.listdir("../../Data/requests-dataset/conversations-request/")
            req = 1
            for folder in folders:
                files = os.listdir("../../Data/requests-dataset/conversations-request/" + folder + "/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/conversations-request/" + folder + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
            req = 1
            l = 1
            while l <= 5:
                requests = {}
                files = os.listdir("../../Data/requests-dataset/conversations-request/iot-requests/"+str(l)+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/conversations-request/iot-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1
                servicesPerGateway = 2000
                while servicesPerGateway <= 10000:
                    register.removeAllServices()
                    register.serviceRegistration(servicesPerGateway)
                    r = 1
                    while r <= 1:
                        currentReq = 1
                        global numberOfResponses
                        numberOfResponses = 0
                        global numberOfRequests
                        numberOfRequests = 0
                        global sendRequest
                        sendRequest = True
                        while(numberOfResponses < limitOfRequests):
                            if sendRequest == True:
                                print("\n*** Approach " + approach + "::Services " + str(servicesPerGateway) + " Length " + str(l) + "::Round " + str(r) + "::Number of Requests: " + str(numberOfRequests + 1) + " ***")
                                try:
                                    request = {}
                                    """if currentReq == req:
                                        currentReq = 1"""
                                    time.sleep(3)
                                    request = requests[randint(1, (req - 1))]
                                    # currentReq = currentReq + 1
                                    global numberOfRequests
                                    numberOfRequests = numberOfRequests + 1
                                    jobId = clientId + str(numberOfRequests)
                                    request["jobId"] = jobId
                                    request["consumerAddress"] = consumerAddress
                                    request["messageType"] = "request"
                                    requestTime = int(round(time.time() * 1000))
                                    request["requestTime"] = requestTime
                                    request["requestSize"] = utf8len(str(request))
                                    request["discoveryTime"] = 0
                                    request["discoveryType"] = discoveryType
                                    request["similarityThreshold"] = similarityThreshold
                                    message = json.dumps(request)
                                    subscribeToMessage(consumerAddress, port, "/discovery/response/+")
                                    publishMessage(sde_address1, port, "/discovery/request", message)                
                                    global sendRequest
                                    sendRequest = False
                                    print("Approach " + approach + "::Round " + str(r) + "::Published Request " + str(numberOfRequests) + ":" + str(currentReq - 1) + "::Request ID " + str(request["id"]))
                                    del request
                                except KeyboardInterrupt:
                                    print ("Exception sending the request")
                                    global sendRequest
                                    sendRequest = True 
                        """ Write results' file at the end."""
                        fileName = "results_" + platform + "_services_" + str(servicesPerGateway) + "_length_" + str(l) + "_" + approach +  "_round_" + str(r)
                        writeResults(fileName,True)
                        r = r + 1
                        while(sendRequest == False):
                            time.sleep(0.1)
                    servicesPerGateway = servicesPerGateway + 2000
                l = l + 1
            register.removeAllServices()
            print("finished")
        else:
            """requests = {}
            folders = os.listdir("../../Data/requests-dataset/json-requests/")
            req = 1
            for folder in folders:
                files = os.listdir("../../Data/requests-dataset/json-requests/" + folder + "/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/json-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
            req = 1
            l = 1
            while l <= 5:
                requests = {}
                files = os.listdir("../../Data/requests-dataset/all-requests/"+str(l)+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1
                functionalThreshold = 10
                while functionalThreshold >= 10:
                    feedbackThreshold = 10
                    while feedbackThreshold >= 0:
                        topK = 25
                        while topK >= 5:
                            servicesPerGateway = 2000
                            while servicesPerGateway <= 10000:
                                register.removeAllServices()
                                register.serviceRegistration(servicesPerGateway)
                                r = 1
                                while r <= 10:
                                    currentReq = 1
                                    global numberOfResponses
                                    numberOfResponses = 0
                                    global numberOfRequests
                                    numberOfRequests = 0
                                    global sendRequest
                                    sendRequest = True
                                    while(numberOfResponses < limitOfRequests):
                                        if sendRequest == True:
                                            print("\n*** Approach " + approach + "::Services " + str(servicesPerGateway) + " Length " + str(l) + " TopK " + str(topK) + "::FeedbackThreshold " + str(feedbackThreshold / 10) + "::FunctionalThreshold " + str(functionalThreshold / 10) + "::Round " + str(r) + "::Number of Requests: " + str(numberOfRequests + 1) + " ***")
                                            try:
                                                request = {}
                                                """if currentReq == req:
                                                    currentReq = 1"""
                                                time.sleep(3)
                                                request = requests[randint(1, (req - 1))]
                                                # currentReq = currentReq + 1
                                                global numberOfRequests
                                                numberOfRequests = numberOfRequests + 1
                                                jobId = clientId + str(numberOfRequests)
                                                request["jobId"] = jobId
                                                request["consumerAddress"] = consumerAddress
                                                request["messageType"] = "request"
                                                requestTime = int(round(time.time() * 1000))
                                                request["requestTime"] = requestTime
                                                request["requestSize"] = utf8len(str(request))
                                                request["discoveryTime"] = 0
                                                request["discoveryType"] = discoveryType
                                                request["similarityThreshold"] = similarityThreshold
                                                request["feedbackThreshold"] = (feedbackThreshold / 10)
                                                request["functionalThreshold"] = (functionalThreshold / 10)
                                                request["topK"] = topK
                                                message = json.dumps(request)
                                                subscribeToMessage(consumerAddress, port, "/discovery/response/+")
                                                publishMessage(sde_address1, port, "/discovery/request", message)                
                                                global sendRequest
                                                sendRequest = False
                                                print("Published Request " + str(numberOfRequests) + ": " + str(currentReq - 1) + "/" + str(request["id"]))
                                                del request
                                            except KeyboardInterrupt:
                                                print ("Exception sending the request")
                                                global sendRequest
                                                sendRequest = True 
                                    """ Write results' file at the end."""
                                    fileName = "results_services_" + platform + "_" + str(servicesPerGateway) + "_length_" + str(l) + "_topK_" + str(topK) + "_" + approach + "_feedt_" + str(feedbackThreshold / 10) + "_" + "_funct_" + str(functionalThreshold / 10) + "_round_" + str(r)
                                    writeResults(fileName,True)
                                    r = r + 1
                                    while(sendRequest == False):
                                        time.sleep(0.1)
                                servicesPerGateway = servicesPerGateway + 2000
                            topK = topK - 5
                        feedbackThreshold = feedbackThreshold - 2
                        del request
                        global sendRequest
                        sendRequest = False
                    functionalThreshold = functionalThreshold - 2
                l = l + 1 
            register.removeAllServices()
            print("finished")
    elif platform == "pi":
        if discoveryType <= 4:
            """requests = {}
            folders = os.listdir("../../Data/requests-dataset/json-requests-tchpc/")
            req = 1
            for folder in folders:
                files = os.listdir("../../Data/requests-dataset/json-requests-tchpc/"+folder+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/json-requests-tchpc/" + folder + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
            req = 1
            l = 4
            while l <= 5:
                req = 1
                requests = {}
                files = os.listdir("../../Data/requests-dataset/all-requests/"+str(l)+"/")
                names = []
                while req<=limitOfRequests:
                    filename = random.choice(files)
                    if filename not in names:
                        names.append(filename)
                        request = {}
                        with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + filename) as dataFile:    
                            request = json.load(dataFile)
                        request["services"] = str(l)
                        request["file"] = filename
                        print(filename)
                        requests[req] = request
                        req = req + 1
                """for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    print("file")
                    requests[req] = request
                    req = req + 1"""
                servicesPerGateway = 500
                if l==4:
                    servicesPerGateway=1500
                while servicesPerGateway <= 2500:
                    request={}
                    request["servicesPerGateway"]=servicesPerGateway
                    request["messageType"]="removeServices"
                    request["consumerAddress"] = consumerAddress
                    message = json.dumps(request)
                    subscribeToMessage(consumerAddress,port,"/discovery/removeServicesACK/")
                    publishMessage(sde_address1,port,"/discovery/request",message)
                    publishMessage(sde_address2,port,"/discovery/request",message)
                    publishMessage(sde_address3,port,"/discovery/request",message)
                    publishMessage(sde_address4,port,"/discovery/request",message)
                    #publishMessage(sde_address5,port,"/discovery/request",message)
                    del request
                    global sendRequest
                    sendRequest = False
                    totalServices = servicesPerGateway * 4
                    r = 1
                    while r <= 1:
                        currentReq = 1
                        global numberOfResponses
                        numberOfResponses = 0
                        global numberOfRequests
                        numberOfRequests = 0
                        """global sendRequest
                        sendRequest = True"""
                        while(numberOfResponses < limitOfRequests):
                            if sendRequest == True:
                                print("\n*** Approach "+approach+"::Services "+str(totalServices) + " Length " + str(l) + " Gateways " + str(numberOfGateways) +"::Number of Requests: " + str(numberOfRequests+1) + " ***")
                                try:
                                    request = {}
                                    if currentReq == req:
                                        currentReq = 1
                                    time.sleep(3)
                                    request = requests[randint(1,(req-1))]
                                    #currentReq = currentReq + 1
                                    global numberOfRequests
                                    numberOfRequests = numberOfRequests + 1
                                    jobId = clientId + str(numberOfRequests)
                                    request["jobId"] = jobId
                                    request["consumerAddress"] = consumerAddress
                                    request["messageType"] = "request"
                                    requestTime = int(round(time.time() * 1000))
                                    request["requestTime"] = requestTime
                                    request["requestSize"] = utf8len(str(request))
                                    request["discoveryTime"] = 0
                                    request["discoveryType"] = discoveryType
                                    request["similarityThreshold"] = similarityThreshold
                                    message = json.dumps(request)
                                    subscribeToMessage(consumerAddress,port,"/discovery/response/+")
                                    publishMessage(sde_address1,port,"/discovery/request",message)                
                                    global sendRequest
                                    sendRequest = False
                                    print("Approach " + approach +"::Round " + str(r) + "::Published Request " + str(numberOfRequests) + ": " +str(currentReq-1) + "/" + str(request["id"]))
                                    del request
                                except KeyboardInterrupt:
                                    print ("Exception sending the request")
                                    global sendRequest
                                    sendRequest = True 
                        """ Write results' file at the end."""
                        fileName = "results_"+platform+"_services_"+str(totalServices) + "_length_" + str(l) + "_gateways_" + str(numberOfGateways) + "_"+approach + "_round_" + str(r)
                        writeResults(fileName,True)
                        r = r + 1
                        while(sendRequest == False):
                            time.sleep(0.1)
                    servicesPerGateway = servicesPerGateway + 500
                l = l + 1
            request={}
            request["messageType"]="remove"
            request["consumerAddress"] = consumerAddress
            message = json.dumps(request)
            print("Waiting for removeACK")
            subscribeToMessage(consumerAddress,port,"/discovery/removeACK/")
            print("Consumer Address:  " + consumerAddress)
            publishMessage(sde_address1,port,"/discovery/request",message)
            publishMessage(sde_address2,port,"/discovery/request",message)
            publishMessage(sde_address3,port,"/discovery/request",message)
            publishMessage(sde_address4,port,"/discovery/request",message)
            #publishMessage(sde_address5,port,"/discovery/request",message)
            del request
            global sendRequest
            sendRequest = False
            print("finished")
        elif discoveryType<=9:
            """requests = {}
            folders = os.listdir("../../Data/requests-dataset/json-requests/")
            req = 1
            for folder in folders:
                files = os.listdir("../../Data/requests-dataset/json-requests/"+folder+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/json-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
            req = 1
            l = 3
            while l <= 5:
                req = 1
                requests = {}
                files = os.listdir("../../Data/requests-dataset/all-requests/"+str(l)+"/")
                names = []
                while req<=limitOfRequests:
                    filename = random.choice(files)
                    if filename not in names:
                        names.append(filename)
                        request = {}
                        with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + filename) as dataFile:    
                            request = json.load(dataFile)
                            request["services"] = str(l)
                            request["file"] = filename
                            print(filename)
                            requests[req] = request
                            req = req + 1
                """files = os.listdir("../../Data/requests-dataset/all-requests/"+str(l)+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
                print("Number of request: " , str(req))
                for k, request in requests.items():
                    print("File: " + str(k)+ "::" + request["file"])
                functionalThreshold = 10
                while functionalThreshold>=10:
                    feedbackThreshold = 10
                    while feedbackThreshold >= 10:
                        topK = 5
                        while topK >= 5:
                            servicesPerGateway = 500
                            while servicesPerGateway <= 2500:
                                request={}
                                request["servicesPerGateway"]=servicesPerGateway
                                request["messageType"]="removeServices"
                                request["consumerAddress"] = consumerAddress
                                message = json.dumps(request)
                                subscribeToMessage(consumerAddress,port,"/discovery/removeServicesACK/")
                                publishMessage(sde_address1,port,"/discovery/request",message)
                                publishMessage(sde_address2,port,"/discovery/request",message)
                                publishMessage(sde_address3,port,"/discovery/request",message)
                                publishMessage(sde_address4,port,"/discovery/request",message)
                                #publishMessage(sde_address5,port,"/discovery/request",message)
                                del request
                                global sendRequest
                                sendRequest = False
                                totalServices = servicesPerGateway * 4
                                r = 1
                                while r <= 5:
                                    currentReq = 1
                                    global numberOfResponses
                                    numberOfResponses = 0
                                    global numberOfRequests
                                    numberOfRequests = 0
                                    """global sendRequest
                                    sendRequest = True"""
                                    while(numberOfResponses < limitOfRequests):
                                        if sendRequest == True:
                                            print("\n*** Approach "+approach+"::Services "+str(totalServices) + " Length" + str(l) + " Gateways " + str(numberOfGateways) +" TopK "+str(topK)+"::FeedbackThreshold "+str(feedbackThreshold/10)+"::FunctionalThreshold "+str(functionalThreshold/10)+"::Round " + str(r) +"::Number of Requests: " + str(numberOfRequests+1) + " ***")
                                            try:
                                                request = {}
                                                if currentReq == req:
                                                    currentReq = 1
                                                time.sleep(3)
                                                #request = requests[randint(1,(req-1))]
                                                request = requests[currentReq]
                                                currentReq = currentReq + 1
                                                global numberOfRequests
                                                numberOfRequests = numberOfRequests + 1
                                                jobId = clientId + str(numberOfRequests)
                                                request["jobId"] = jobId
                                                request["consumerAddress"] = consumerAddress
                                                request["messageType"] = "request"
                                                requestTime = int(round(time.time() * 1000))
                                                request["requestTime"] = requestTime
                                                request["requestSize"] = utf8len(str(request))
                                                request["discoveryTime"] = 0
                                                request["discoveryType"] = discoveryType
                                                request["similarityThreshold"] = similarityThreshold
                                                request["feedbackThreshold"] = (feedbackThreshold/10)
                                                request["functionalThreshold"] = (functionalThreshold/10)
                                                request["topK"] = topK
                                                message = json.dumps(request)
                                                subscribeToMessage(consumerAddress,port,"/discovery/response/+")
                                                publishMessage(sde_address1,port,"/discovery/request",message)                
                                                global sendRequest
                                                sendRequest = False
                                                print("Approach " + approach +"::Round " + str(r) + "::Published Request " + str(numberOfRequests) + ":" +str(currentReq-1) + "::Request ID " + str(request["id"]))
                                                del request
                                            except KeyboardInterrupt:
                                                print ("Exception sending the request")
                                                global sendRequest
                                                sendRequest = True 
                                    """ Write results' file at the end."""
                                    fileName = "results_"+platform+"_services_"+str(totalServices)+ "_length_" + str(l) + "_gateways_" + str(numberOfGateways) +  "_topK_" + str(topK) +"_"+approach + "_feedt_" + str(feedbackThreshold/10)+"_"+ "_funct_" + str(functionalThreshold/10)+"_round_" + str(r)
                                    writeResults(fileName,True)
                                    r = r + 1
                                    while(sendRequest == False):
                                        time.sleep(0.1)
                                servicesPerGateway = servicesPerGateway + 500
                            topK = topK - 10
                        feedbackThreshold = feedbackThreshold - 2
                    functionalThreshold = functionalThreshold - 2
                l = l + 1
            request={}
            request["messageType"]="remove"
            request["consumerAddress"] = consumerAddress
            message = json.dumps(request)
            print("Waiting for removeACK")
            subscribeToMessage(consumerAddress,port,"/discovery/removeACK/")
            print("Consumer Address:  " + consumerAddress)
            publishMessage(sde_address1,port,"/discovery/request",message)
            publishMessage(sde_address2,port,"/discovery/request",message)
            publishMessage(sde_address3,port,"/discovery/request",message)
            publishMessage(sde_address4,port,"/discovery/request",message)
            #publishMessage(sde_address5,port,"/discovery/request",message)
            del request
            global sendRequest
            sendRequest = False 
            print("finished")
        else:
            """requests = {}
            folders = os.listdir("../../Data/requests-dataset/conversations-request/")
            req = 1
            for folder in folders:
                files = os.listdir("../../Data/requests-dataset/conversations-request/"+folder+"/")
                for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/conversations-request/" + folder + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""
            req = 1
            l = 1
            while l <= 5:
                req = 1
                requests = {}
                files = os.listdir("../../Data/requests-dataset/conversations-request/iot-requests/"+str(l)+"/")
                names = []
                while req<=limitOfRequests:
                    filename = random.choice(files)
                    if filename not in names:
                        names.append(filename)
                        request = {}
                        with open("../../Data/requests-dataset/conversations-request/iot-requests/" + str(l) + "/" + filename) as dataFile:    
                            request = json.load(dataFile)
                        request["services"] = str(l)
                        request["file"] = filename
                        print(filename)
                        requests[req] = request
                        req = req + 1
                """for file in files:
                    request = {}
                    with open("../../Data/requests-dataset/all-requests/" + str(l) + "/" + file) as dataFile:    
                        request = json.load(dataFile)
                    request["services"] = str(l)
                    request["file"] = file
                    requests[req] = request
                    req = req + 1"""        
            
                servicesPerGateway = 500
                while servicesPerGateway <= 2500:
                    request={}
                    request["servicesPerGateway"]=servicesPerGateway
                    request["messageType"]="removeServices"
                    request["consumerAddress"] = consumerAddress
                    message = json.dumps(request)
                    subscribeToMessage(consumerAddress,port,"/discovery/removeServicesACK/")
                    publishMessage(sde_address1,port,"/discovery/request",message)
                    publishMessage(sde_address2,port,"/discovery/request",message)
                    publishMessage(sde_address3,port,"/discovery/request",message)
                    publishMessage(sde_address4,port,"/discovery/request",message)
                    #publishMessage(sde_address5,port,"/discovery/request",message)
                    del request
                    global sendRequest
                    sendRequest = False
                    totalServices = servicesPerGateway * 4
                    r = 1
                    while r <= 1:
                        currentReq = 1
                        global numberOfResponses
                        numberOfResponses = 0
                        global numberOfRequests
                        numberOfRequests = 0
                        """global sendRequest
                        sendRequest = True"""
                        while(numberOfResponses < limitOfRequests):
                            if sendRequest == True:
                                print("\n*** Approach "+approach+"::Services "+str(totalServices)+ " Length " + str(l) + " Gateways " + str(numberOfGateways) +"::Number of Requests: " + str(numberOfRequests+1) + " ***")
                                try:
                                    request = {}
                                    """if currentReq == req:
                                        currentReq = 1"""
                                    time.sleep(3)
                                    request = requests[randint(1,(req-1))]
                                    #currentReq = currentReq + 1
                                    global numberOfRequests
                                    numberOfRequests = numberOfRequests + 1
                                    jobId = clientId + str(numberOfRequests)
                                    request["jobId"] = jobId
                                    request["consumerAddress"] = consumerAddress
                                    request["messageType"] = "request"
                                    requestTime = int(round(time.time() * 1000))
                                    request["requestTime"] = requestTime
                                    request["requestSize"] = utf8len(str(request))
                                    request["discoveryTime"] = 0
                                    request["discoveryType"] = discoveryType
                                    request["similarityThreshold"] = similarityThreshold
                                    message = json.dumps(request)
                                    subscribeToMessage(consumerAddress,port,"/discovery/response/+")
                                    publishMessage(sde_address1,port,"/discovery/request",message)                
                                    global sendRequest
                                    sendRequest = False
                                    print("Approach " + approach +"::Round " + str(r) + "::Published Request " + str(numberOfRequests) + ": " +str(currentReq-1) + "/" + str(request["id"]))
                                    del request
                                except KeyboardInterrupt:
                                    print ("Exception sending the request")
                                    global sendRequest
                                    sendRequest = True 
                        """ Write results' file at the end."""
                        fileName = "results_"+platform+"_services_"+str(totalServices)+ "_length_" + str(l) + "_gateways_" + str(numberOfGateways) + "_"+approach + "_round_" + str(r)
                        writeResults(fileName,True)
                        r = r + 1
                        while(sendRequest == False):
                            time.sleep(0.1)
                    servicesPerGateway = servicesPerGateway + 500
                l = l + 1
            request={} 
            request["messageType"]="remove"
            request["consumerAddress"] = consumerAddress
            message = json.dumps(request)
            print("Waiting for removeACK")
            subscribeToMessage(consumerAddress,port,"/discovery/removeACK/")
            print("Consumer Address:  " + consumerAddress)
            publishMessage(sde_address1,port,"/discovery/request",message)
            publishMessage(sde_address2,port,"/discovery/request",message)
            publishMessage(sde_address3,port,"/discovery/request",message)
            publishMessage(sde_address4,port,"/discovery/request",message)
            #publishMessage(sde_address5,port,"/discovery/request",message)
            del request
            global sendRequest
            sendRequest = False
            print("finished")

if __name__== "__main__":
    feedt = 0
    tk = 0
    funt = 0 
    nser = 0
    length = 0
    if len(sys.argv) >= 2:
        feedt = int(sys.argv[1])
        tk = int(sys.argv[2])
        funt = int(sys.argv[3])
        nser = int(sys.argv[4])
        length = int(sys.argv[5])
    else:
        feedt = 10
        tk = 25
        funt = 10
        ser = 2000
        length = 1
    main(feedt, tk, funt, nser,length)
