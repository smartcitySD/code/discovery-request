#!/bin/sh
#SBATCH -n 12
#SBATCH -t 48:00:00 
#SBATCH -p compute   # partition name
#SBATCH -J farming_cabrerac # sensible name for the job

# load the modules
source /etc/profile.d/modules.sh
module load staskfarm
module load cports Python/3.5.2-gnu

# execute the commands via the slurm task farm wrapper
staskfarm commands.txt
