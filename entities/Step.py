# *
# * Author: Christian Cabrera
# * Abstract class that represents a step in a service plan
# 
class Step(object):
    
    def getInputs(self):
        return self._inputs

    def setInputs(self, inputs):
        self._inputs = inputs

    def getOutputs(self):
        return self._outputs

    def setOutputs(self, outputs):
        self._outputs = outputs

    def getOrder(self):
        return self._order

    def setOrder(self, order):
        self._order = order

    def getType(self):
        return self._type

    def setType(self, type):
        self._type = type