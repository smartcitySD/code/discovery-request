# *
# * Author: Christian Cabrera
# * Class that represents the location of a service
# 
class Location(object):
    def __init__(self, description, region, negotiable, negotiationConstraints, coordinate):
        self.setDescription(description)
        self.setRegion(region)
        self.setNegotiable(negotiable)
        self.setNegotiationConstraints(negotiationConstraints)
        self.setCoordinate(coordinate)

    def getDescription(self):
        return self._description

    def setDescription(self, description):
        self._description = description

    def getRegion(self):
        return self._region

    def setRegion(self, region):
        self._region = region

    def getNegotiable(self):
        return self._negotiable

    def setNegotiable(self, negiotable):
        self._negotiable = negiotable

    def getNegotiationConstraints(self):
        return self._negotiationConstraints

    def setNegotiationConstraints(self, negotiationConstraints):
        self._negotiationConstraints = negotiationConstraints

    def getCoordinate(self):
        return self._coordinate

    def setCoordinate(self, coordinate):
        self._coordinate = coordinate