# *
# * Author: Christian Cabrera
# * This class represents a sequential step in a plan
# 
from entities.ComposedStep import ComposedStep

class SequentialStep(ComposedStep):
    
    def __init__(self, inputs=[], outputs=[], order=[], steps={}):
        self.reset()
        self.setInputs(inputs)
        self.setOutputs(outputs)
        self.setOrder(order)
        self.setSteps(steps)
        self.setType("SequentialStep")
    
    def reset(self):
        self.setInputs([])
        self.setOutputs([])
        self.setRemainingOutputs([])
        self.setSolvedOutputs([])
        self.setOrder(0)
        self.setSteps({})
        self.setType("")
        
    def getRemainingOutputs(self):
        return self._remainingOutputs

    def setRemainingOutputs(self, remainingOutputs):
        self._remainingOutputs = remainingOutputs
    
    def getSolvedOutputs(self):
        return self._solvedOutputs

    def setSolvedOutputs(self, solvedOutputs):
        self._solvedOutputs = solvedOutputs
        