# *
# * Author: Christian Cabrera
# * This class represents a service plan produced in the discovery process
# 

import json

class Plan(object):
    def __init__(self, inputs=[], outputs=[], state=0, steps={}, domains=[], services=0):
        self.reset()
        self.setInputs(inputs)
        self.setOutputs(outputs)
        self.setState(state)
        self.setSteps(steps)
        self.setDomains(domains)
        self.setServices(services)
    
    def reset(self):
        self.setInputs([])
        self.setOutputs([])
        self.setState(0)
        self.setSteps({})
        self.setDomains([])
        self.setServices(0)

    def getInputs(self):
        return self._inputs

    def setInputs(self, inputs):
        self._inputs = inputs

    def getOutputs(self):
        return self._outputs

    def setOutputs(self, outputs):
        self._outputs = outputs

    def getState(self):
        return self._state

    def setState(self, state):
        self._state = state

    def getSteps(self):
        return self._steps

    def setSteps(self, steps):
        self._steps = steps

    def getDomains(self):
        return self._domains

    def setDomains(self, domains):
        self._domains = domains
        
    def getServices(self):
        return self._services

    def setServices(self, services):
        self._services = services
        
        
        