# *
# * Author: Christian Cabrera
# * This entity represents a service description in the service model
# 
from System import *
from System.Collections.Generic import *
from System.Collections import *

class ServiceDescription(object):
    def __init__(self, id, type, url, name, description, provider, state, negotiable, supportSLA, samplingPeriods, temporalities, location, inputs, outputs, domains, qosParameters, aggregation, dataReporting):
        self.setId(id)
        self.setType(type)
        self.setUrl(url)
        self.setName(name)
        self.setDescription(description)
        self.setProvider(provider)
        self.setState(state)
        self.setNegotiable(negotiable)
        self.setSupportSLA(supportSLA)
        self.setSamplingPeriods(samplingPeriods)
        self.setTemporalities(temporalities)
        self.setLocation(location)
        self.setInputs(inputs)
        self.setOutputs(outputs)
        self.setDomains(domains)
        self.setQosParameters(qosParameters)
        self.setAggregation(aggregation)
        self.setDataReporting(dataReporting)

    def getType(self):
        return self._type

    def setType(self, type):
        self._type = type

    def getUrl(self):
        return self._url

    def setUrl(self, url):
        self._url = url

    def getName(self):
        return self._name

    def setName(self, name):
        self._name = name
    
    def getDescription(self):
        return self._description

    def setName(self, description):
        self._description = description
        
    def getProvider(self):
        return self._provider

    def setProvider(self, provider):
        self._provider = provider

    def getInputs(self):
        return self._inputs

    def setInputs(self, inputs):
        self._inputs = inputs

    def getDomains(self):
        return self._domains

    def setDomains(self, domains):
        self._domains = domains

    def getOutputs(self):
        return self._outputs

    def setOutputs(self, outputs):
        self._outputs = outputs

    def getQosParameters(self):
        return self._qosParameters

    def setQosParameters(self, qosParameters):
        self._qosParameters = qosParameters

    def getId(self):
        return id

    def setId(self, id):
        self._id = id

    def getDescription(self):
        return description

    def setDescription(self, description):
        self._description = description

    def isState(self):
        return self._state

    def setState(self, state):
        self._state = state

    def getNegotiable(self):
        return self._negotiable

    def setNegotiable(self, negotiable):
        self._negotiable = negotiable

    def getSupportSLA(self):
        return self._supportSLA

    def setSupportSLA(self, supportSLA):
        self._supportSLA = supportSLA

    def getSamplingPeriods(self):
        return self._samplingPeriods

    def setSamplingPeriods(self, samplingPeriods):
        self._samplingPeriods = samplingPeriods

    def getTemporalities(self):
        return self._temporalities

    def setTemporalities(self, temporalities):
        self._temporalities = temporalities

    def getLocation(self):
        return self._location

    def setLocation(self, location):
        self._location = location

    def getAggregation(self):
        return self._aggregation

    def setAggregation(self, aggregation):
        self._aggregation = aggregation

    def getDataReporting(self):
        return self._dataReport