# *
# * Author: Christian Cabrera
# * This entity represents a service response in the service model
# 
from System import *
from System.Collections.Generic import *
from System.Collections import *

class ServiceResponse(object):
    def __init__(self, jobId, inputs, outputs, domains, qosParameters):
        self.setJobId(jobId)
        self.setInputs(inputs)
        self.setOutputs(outputs)
        self.setDomains(domains)
        self.setQosParameters(qosParameters)

    def getInputs(self):
        return self._inputs

    def setInputs(self, inputs):
        self._inputs = inputs

    def getOutputs(self):
        return self._outputs

    def setOutputs(self, outputs):
        self._outputs = outputs

    def getQosParameters(self):
        return self._qosParameters

    def setQosParameters(self, qosParameters):
        self._qosParameters = qosParameters

    def setJobId(self, jobId):
        self._jobId = jobId

    def getJobId(self):
        return self._jobId

    def getDomains(self):
        return self._domains

    def setDomains(self, domains):
        self._domains = domains