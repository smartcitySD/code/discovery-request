# *
# * Author: Christian Cabrera
# * Class that represents a negotiation constraint
# 
class NegotiationConstraint(object):
    def __init__(self, type):
        self.setType(type)

    def getType(self):
        return self._type

    def setType(self, type):
        self._type = type