# *
# * Author: Christian Cabrera
# * Class that represents coordinates of a location
# 
class Coordinate(object):
    def __init__(self, x, y, z):
        self.setX(x)
        self.setY(y)
        self.setZ(z)

    def getX(self):
        return self._x

    def setX(self, x):
        self._x = x

    def getY(self):
        return self._y

    def setY(self, y):
        self._y = y

    def getZ(self):
        return self._z

    def setZ(self, z):
        self._z = z
