# *
# * Author: Christian Cabrera
# * This entity represents a domain in the service description
# 
class Domain(object):
    def __init__(self, name, url):
        self.setName(name)
        self.setUrl(url)

    def getName(self):
        return self._name

    def setName(self, name):
        self._name = name

    def getUrl(self):
        return self._url

    def setUrl(self, url):
        self._url = url