# *
# * Author: Christian Cabrera
# * This entity represents a QoS parameter in the service description
# 
class QoSParameter(object):
    def __init__(self, name, metric, value, unit, negotiable, expression, weight, negotiationConstraints):
        self.setName(name)
        self.setMetric(metric)
        self.setValue(value)
        self.setUnit(unit)
        self.setNegotiable(negotiable)
        self.setExpression(expression)
        self.setWeight(weight)
        self.setNegotiationConstraints(negotiationConstraints)

    def getName(self):
        return self._name

    def setName(self, name):
        self._name = name

    def getUnit(self):
        return self._unit

    def setUnit(self, unit):
        self._unit = unit

    def getValue(self):
        return self._value

    def setValue(self, value):
        self._value = value

    def getMetric(self):
        return self._metric

    def setMetric(self, metric):
        self._metric = metric

    def getNegotiable(self):
        return self._negotiable

    def setNegotiable(self, negotiable):
        self._negotiable = negotiable

    def getExpression(self):
        return self._expression

    def setExpression(self, expression):
        self._expression = expression

    def getWeight(self):
        return self._weight

    def setWeight(self, weight):
        self._weight = weight

    def getNegotiationConstraints(self):
        return self._negotiationConstraints

    def setNegotiationConstraints(self, negotiationConstraints):
        self._negotiationConstraints = negotiationConstraints