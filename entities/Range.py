# *
# * Author: Christian Cabrera
# * Class that represents a range for a negotiation constraint
# 
class Range(object):
    NegotiationConstraint = property()

    def __init__(self, type, min, max):
        self.super(type)
        self.setMin(min)
        self.setMax(max)

    def getMin(self):
        return self._min

    def setMin(self, min):
        self._min = min

    def getMax(self):
        return self._max

    def setMax(self, max):
        self._max = max