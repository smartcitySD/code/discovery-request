# * Author: Christian Cabrera
# * Class that represents the data reporting of a service
# 
class DataReporting(object):
    def __init__(self, dataPeriod, condition):
        self.setDataPeriod("")
        self.setCondition(condition)

    def getDataPeriod(self):
        return self._dataPeriod

    def setDataPeriod(self, dataPeriod):
        self._dataPeriod = dataPeriod

    def getCondition(self):
        return self._condition
    
    def setDataPeriod(self, condition):
        self._condition = condition