# *
# * Author: Christian Cabrera
# * This class represents a sequential step in a plan
# 
from entities.ComposedStep import ComposedStep

class ParallelStep(ComposedStep):
    
    def __init__(self, inputs=[], outputs=[], order=0, steps={}):
        self.reset()
        self.setInputs(inputs)
        self.setOutputs(outputs)
        self.setOrder(order)
        self.setSteps(steps)
        self.setType("ParallelStep")
    
    def reset(self):
        self.setInputs([])
        self.setOutputs([])
        self.setRemainingOutputs([])
        self.setSolvedOutputs([])
        self.setOrder(0)
        self.setSteps({})
        self.setType("")
        
    def getRemainingOutputs(self):
        return self._remainingOutputs

    def setRemainingOutputs(self, remainingOutputs):
        self._remainingOutputs = remainingOutputs
    
    def getSolvedOutputs(self):
        return self._solvedOutputs

    def setSolvedOutputs(self, solvedOutputs):
        self._solvedOutputs = solvedOutputs    
        
    