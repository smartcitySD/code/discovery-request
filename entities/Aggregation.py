# * Author: Christian Cabrera
# * Class that represents the aggregation function of a service
# 
class Aggregation(object):
    def __init__(self,function, perNode, window):
        self.setFunction(function)
        self.setPerNode(perNode)
        self.setWindow(window)

    def getFunction(self):
        return self._function

    def setFunction(self, function):
        self._function = function

    def getPerNode(self):
        return self._perNode

    def setPerNode(self, perNode):
        self._perNode = perNode

    def getWindow(self):
        return self._window

    def setWindow(self, window):
        self._window = window