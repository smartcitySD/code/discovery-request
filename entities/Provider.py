# *
# * Author: Christian Cabrera
# * Class that represents a service provider
# 
class Provider(object):
    def __init__(self, providerId, name, url, type):
        self.setProviderId(providerId)
        self.setName(name)
        self.setUrl(url)
        self.setType(type)

    def getProviderId(self):
        return self._providerId

    def setProviderId(self, providerId):
        self._providerId = providerId

    def getName(self):
        return self._name

    def setName(self, name):
        self._name = name

    def getUrl(self):
        return self._url

    def setUrl(self, url):
        self._url = url

    def getType(self):
        return self._type

    def setType(self, type):
        self._type = type