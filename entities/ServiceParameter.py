# *
# * Author: Christian Cabrera
# * This entity represents a service parameter (i.e., input or output) in the service description
# 
class ServiceParameter(object):
    def __init__(self, name, type, description, unit, value):
        self.setName(name)
        self.setType(type)
        self.setDescription(description)
        self.setUnit(unit)
        self.setValue(value)

    def getName(self):
        return self._name

    def setName(self, name):
        self._name = name

    def getType(self):
        return self._type

    def setType(self, type):
        self._type = type

    def getDescription(self):
        return self._description

    def setDescription(self, description):
        self._description = description

    def getUnit(self):
        return self._unit

    def setUnit(self, unit):
        self._unit = unit

    def getValue(self):
        return self._value

    def setValue(self, value):
        self._value = value