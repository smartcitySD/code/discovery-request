# *
# * Author: Christian Cabrera
# * Class that represents an enumeration for a negotiation constraint
# 
class Enumeration(object):
    NegotiationConstraint = property()

    def __init__(self, type, values):
        self.super(type)
        self.setValues(values)

    def getValues(self):
        return self._values

    def setValues(self, values):
        self._values = values