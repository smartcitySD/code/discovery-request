# *
# * Author: Christian Cabrera
# * This class represents a sequential step in a plan
# 

from entities.Step import Step

class SimpleStep(Step):
    
    def __init__(self, inputs=[], outputs=[], order=0, serviceDescription={}):
        self.reset()
        self.setInputs(inputs)
        self.setOutputs(outputs)
        self.setOrder(order)
        self.setType("SimpleStep")
        self.setServiceDescription(serviceDescription)
    
    def reset(self):
        self.setInputs([])
        self.setOutputs([])
        self.setOrder(0)
        self.setType("")
        self.setServiceDescription({})
    
    def getServiceDescription(self):
        return self._serviceDescription

    def setServiceDescription(self, serviceDescription):
        self._serviceDescription = serviceDescription